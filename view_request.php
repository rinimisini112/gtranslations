<?php 
include "inc/headerblank.php";
?>
<head>
    <link rel="stylesheet" href="css/dashboard.css">
</head>

<?php
$userid = $_SESSION['userid'];

if (isset($_GET['id'])) {
    $request = getSpecificRequest($_GET['id']);
    $_SESSION['documentid'] = $_GET['id'];
    $userName = $request['name'] . ' ' . $request['surname'];
    $usersId= $request['userid'];
    $_SESSION['clientid'] = $usersId;
    $file = $request['document_file'];
    $language = $request['language'];
    $sumbsission_date = $request['submission_date'];
    $deadline = $request['deadline'];
    $details = $request['request_details'];
    $requestId = $request['documentid'];  
    $status = $request['status'];
}
   ?>
<section class="file-progress">
    <h1 class="progress-title">Viewing Project NR.<?php echo $requestId?></h1>
   <?php 
    if($status == 1) {
        echo "<p class='priority'>Priority!!!, this document has been resent by the user </p>";        
    }
     echo "<p>User Name: <span> $userName<span></p>";
     echo "<p>Request Details: <span>  $details<span></p>";
     echo "<p>Submission Date: <span>$sumbsission_date<span></p>";
     echo "<p>Deadline: <span> $deadline</p>";
     echo "<p>Language: <span>$language<span></p>";
   
     if ($file) {
        $filename = basename($file); 
        $filePath = "uploads/" . $filename;
            echo "<a href='$filePath' download>Download Document</a>";        
    } else {
        echo "Document not available.";
    }
   ?>
   <a href="dashboard.php" class="go_back-button">Go Back</a>
</section>
<script>
     function downloadDocument(filename) {

        window.location.href = 'download_document.php?filename=' + encodeURIComponent(filename);
    }
</script>