
<link rel="stylesheet" href="css/footer.css">
<footer id="footer-section">
      <div class="socials-container">
        <div class="social-icons">
          <i class='bx bxl-twitter'></i>
          <i class='bx bxl-facebook-circle'></i>
          <i class='bx bxl-instagram' ></i>
          <i class='bx bx-globe' ></i>
          <i class='bx bxl-github' ></i>
        </div>
        <div class="social-credentials">
          <ul>
            <li><a href="">Work</a></li>
            <li><a href="">Story</a></li>
            <?php
            if (isset($_SESSION['user']) && isset($_SESSION['user']['role'])) {
              if ($_SESSION['user']['role'] != 1 && $_SESSION['user']['role'] != 2) {
                echo "<li><a href='packages.php'>Services</a></li>";
              }
          } else {
            echo "<li><a href='services.php'>Services</a></li>";
          }
            ?>
          </ul>
          <ul>
            <li><a href="">Careers</a></li>
            <li><a href="">Contact Us</a></li>
          </ul>
        </div>
      </div>
      <div class="info-container">
        <div class="top-container">
        <div class="contact-list">
          <p class="quick-contact">Quick Contact.</p>
          <ul>
            <li><i class='bx bxs-home-heart' ></i> Dardania SU-9/2</li>
            <li><i class='bx bxs-phone'></i> +383 45 535 266</li>
            <li><i class='bx bx-envelope'></i> misinirini@gmail.com</li>
          </ul>
          <p class="newsletter">Subscribe to our newsletter to Stay notified</p>
         <?php
          /*if (isset($_POST['submitNewsletter'])) {
          newsletter($_POST['email']);
          }
          sendNewsletter();*/
         ?>
          <form action="" method="POST">
          <input type="email" name="email" id="email" placeholder="Your Email">
          <input type="submit" name="submitNewsletter" value="Subscribe">
        </form>
        </div>
        <div class="blog-spot">
    <p class="headline">View our blog and get notified about news regarding our website</p>

    <?php
$latestPosts = getLatestBlogPosts();
$blogImage = $latestPosts[0]['picture_path']; // Assuming you want the picture_path of the first post

foreach ($latestPosts as $post) {
    echo '<div class="blog-box">';
    if (!empty($post['picture_path'])) {
        echo "<img src='{$post['picture_path']}' alt=''>";
    } else {
        echo '<img src="images/blog-img.png" alt="">';
    }
        echo '<article>';
        echo "<p class='blog-title'>{$post['title']}</p>";
        echo "<p class='blog-date'>Published on: {$post['created_at']}</p>";
        echo "<p class='blog-prev'>" . substr($post['content'], 0, 80) . "...</p>";
        echo '</article>';
        echo '</div>';
    }
    ?>
</div>
        </div>
        <div class="trademark-copyright">
          <p class="trademark">All rights reserved by <span>G Translations and Rini Misini &copy;</span></p>
        </div>
      </div>
    </footer>
    <script>
      /*funksioni per shfaqjen e dates se sotit*/ 
      let date = document.querySelectorAll('.blog-date');
      let currentDate = new Date();
      date.forEach(date => {
        date.innerHTML = currentDate;
      })

   window.addEventListener('scroll', function() {
  let scrolled = window.pageYOffset;
  let wrapper = document.getElementById('wrapper');
  let header = document.getElementById('header-container');
  let secondWrapper = document.getElementById('second-wrapper');
  let thirdWrapper = document.getElementById('third-wrapper');
  
  wrapper.style.backgroundPosition = 'center ' + (-scrolled * 0.1) + 'px';
  header.style.backgroundPosition = 'center ' + (-scrolled * 0.3) + 'px';
  secondWrapper.style.backgroundPosition = 'center ' + (-scrolled * 0.1) + 'px';
  thirdWrapper.style.backgroundPosition = 'center ' + (-scrolled * 0.05) + 'px';
  let parallaxSpeed = 0.2; 
  
});  



  $(".title").css("left", "-50px");
$('#header-title').css("opacity", "0").css('left','20rem');
window.setTimeout(() => {
  $(document).ready(function() {
     $(".title").animate({ opacity: 1, left: "10px" }, 1500);
    $('#header-title').animate({ opacity: 1, left: "42rem" }, 1500);
  });
}, 1000);

$('#logout').click(function(event) {
        $.ajax({
            url: 'functions.php?argument=logout',
            success: function(data) {
                window.location.href = data;
            },
            error: function(xhr, status, error) {
                console.log(error);
            }
        });
    });

    </script>
</body>
</html>