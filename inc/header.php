<?php
include "functions.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>G Translations</title>
    <link rel="stylesheet" href="css/header.css">
    <script src="java-jquery/jquery.js"></script>
    <script src="java-jquery/jquery.validate.js"></script>
    <script src="java-jquery/jquery.validate.min.js"></script>
    <script src="java-jquery/jquery-ui.js"></script>
    <script src="java-jquery/minified/ScrollTrigger.min.js"></script>
    <script src="java-jquery/minified/gsap.min.js"></script>
    <link href='https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
    <style>
@import url('https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;500;700&family=Oswald:wght@600&family=Playfair+Display:wght@400;600;700&family=Roboto+Mono:wght@700&family=Roboto+Slab:wght@300;400;700&family=Roboto:wght@300;500;700&family=Slabo+27px&family=Source+Sans+3:wght@300&family=Ysabeau+SC:wght@300;600&display=swap');
</style>
</head>
<body>
<header id="header-container">
    <nav id="nav-logo">
        <p class="title"><a href="index.php">G Translations</a><span>.</span></p>                                            
</nav>
    <h1 id="header-title">Discover new horizons, Transcend Language Barriers</h1>
  </header>
  <i class="fa-solid fa-bars" onclick="toggleSidebar()" id="hamburger_menu"></i>
  <div class="sidebar">
    <i class="fa-solid fa-bars" onclick="toggleSidebar()"><span>G-Trans</span></i> 
<?php 
generateNavigation();
?>
</nav>
  </div>
  <script>
  function toggleSidebar() {
  const sidebar = document.querySelector('.sidebar');
  sidebar.classList.toggle('active');
}
  </script>