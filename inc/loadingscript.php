</body>
<script>
$('#logout').click(function(event) {
        $.ajax({
            url: 'functions.php?argument=logout',
            success: function(data) {
                window.location.href = data;
            },
            error: function(xhr, status, error) {
                console.log(error); // For debugging purposes
            }
        });
    });
    $(document).ready(function() {
    // Show the dropdown when the "Contact Us" link is clicked
    $('.dropdown').click(function() {
      $('.dropdown-content').toggle();
    });

    // Hide the dropdown when the user clicks outside the dropdown
    $(document).click(function(event) {
      if (!$(event.target).closest('.dropdown-wrapper').length) {
        $('.dropdown-content').hide();
      }
    });
  });
  function toggleSidebar() {
  const sidebar = document.querySelector('.sidebar');
  sidebar.classList.toggle('active');
}
$(document).ready(function() {
  function closePopUp() {
    $('#closePopUp').fadeOut();
  }

  $('.closeMesagge').click(function() {
    closePopUp();
  });
});
</script>
</html>