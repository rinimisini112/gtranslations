<?php 
include "inc/headerblank.php";
?>
<head>
    <link rel="stylesheet" href="css/admin.css">
    <style>
        .sidebar .first_menu li:nth-child(1) a  {
            border-bottom: 2px solid white;
        }
        body {
            overflow-y: unset;
        }
        .translator_wrapper {
            justify-content: right;
            position: relative;
        }
    </style>
</head>
<nav class="staff_nav">
<p class="staff_title">Two step - registration process.</p>
<a href="staff.php">Go Back</a>
</nav>
<div class="translator_wrapper">
    <p class="process_info">Step 1 - Register translator to website.</p>
<div class="user-details">
    <form method="POST" id="register_translator">
        <div class="form-grid">
            <div class="form-col">
                <div class="form-row">
                    <label for="name">Name :</label>
                    <input type="text" name="name" value="<?php if(!empty($name)) echo $name; ?>"/>
                </div>
                <div class="form-row">
                    <label for="surname">Surname :</label>
                    <input type="text" name="surname" value="<?php if(!empty($surname)) echo $surname; ?>"/>
                </div>
                <div class="form-row">
                    <label for="birthdate">Date Of Birth :</label>
                    <input type="date" name="birthdate" value="<?php if(!empty($birthdate)) echo $birthdate; ?>"/>
                </div>
                <div class="form-row">
                    <label for="personalnr">Personal Number :</label>
                    <input type="tel" name="personalnr" id="personalnr" value="<?php if(!empty($personalnr)) echo $personalnr; ?>"/>
                </div>
            </div>
            <div class="form-col">
                <div class="form-row">
                    <label for="phone">Phone Number :</label>
                    <input type="tel" name="phone" id="phone" value="<?php if(!empty($phone)) echo $phone; ?>"/>
                </div>
                <div class="form-row">
                    <label for="email">Email :</label>
                    <input type="email" name="email" id="" value="<?php if(!empty($email)) echo $email; ?>"/>
                </div>
                <div class="form-row">
                    <label for="password">Password :</label>
                    <input type="text" name="password" value="<?php if(!empty($password)) echo $password; ?>"/>
                </div>
                <input type="submit" name="register" value="Register" id="save">
            </div>
            
        </div>
    </form>
    <?php 
            if(isset($_POST['register'])) {
                $staffName = $_POST['name'];
                $staffSurname = $_POST['surname'];
                $staffBirthdate = $_POST['birthdate'];
                $staffPersonalNr = $_POST['personalnr'];
                $staffPhone = $_POST['phone'];
                $staffEmail = $_POST['email'];
                $password = $_POST['password'];
                registerTranslator($staffName,
                $staffSurname,$staffBirthdate,
                $staffPersonalNr,$staffPhone,
                $staffEmail,$password,);
                 
                if (isset($_SESSION['new_translator_userid'])) {
                  $translatorUserId = $_SESSION['new_translator_userid'];
              } else {
                  $translatorUserId = 0;
              }
              echo "<div class='message' id='closePopUp'><p>Step 1 completed. Continue with step 2!</p>";
              // Pass the translatorUserId as a query parameter in the URL
              echo "<button class='closeMesagge'><a href='insert_translator_step2.php?id=$translatorUserId'>Continue!</a></button>";
              echo "</div>";
            }
                ?>
</div>
    </div>
    <script>
        $(document).ready(function () {
          $.validator.addMethod("alphabetsOnly", function(value, element) {
      return /^[a-zA-Z\s]+$/.test(value);
    }, "Enter letters only.");


          // Add validation rules and messages to your registration form
          $("#register_translator").validate({
            rules: {
              name: {
                required: true,
                minlength:2,
                alphabetsOnly: true,
              },
              surname: {
                required: true,
                minlength:2,
                alphabetsOnly: true,
              },
              birthdate: {
                required: true,
              },
              email: {
                required: true,
                email: true,
              },
              phone: {
                required: true,
                number: true,
                maxlength:15,
              },
              personalnr: {
                required: true,
                number: true,
              },
              password: {
                minlength: 6,
                maxlength: 15,
                required: true,
              },
            },
            messages: {
              name: {
                required: "Please enter a Name",
              },
              surname: {
                required: "Please enter a Surname",
              },
              birthdate: {
                required: "Please enter an date of birth",
              },
              email: {
                required: "Please enter an email address",
                email: "Enter a valid email address",
              },
              phone: {
                required: "Enter an telephone number",
                number: "Enter a valid telephone number",
              },
              personalnr: {
                required: "Please enter a personal number",
                number: "Please enter a valid personal number",
              },
              password: {
                required: "Please enter a password",
              },
            },
            errorPlacement: function (error, element) {
                // Place the error message below the input element
                error.insertAfter(element);
            }
        });
          });
    </script>