<?php 
include "inc/headerblank.php";
?>
<head>
    <link rel="stylesheet" href="css/blog.css">
    <style>
       .sidebar .first_menu li:nth-child(3) a  {
            border-bottom: 2px solid white;
        }
        body {
            overflow-y: unset;
        }
        .message {
            height: 3rem;
            width: 100%;
            font-weight: 700;
            font-size: 1.5rem;
            font-family: Arial, Helvetica, sans-serif;
            display: flex;
            align-items: center;
            background-color: white;
            color: black;
            padding-left: 1rem;
        }
        .message a {
            padding-left: 1rem;
            color: black;
            font-weight: 700;
            font-size: 1.5rem;
            font-family: Arial, Helvetica, sans-serif;
        }
    </style>
</head>
<section class="blog-container">
    <nav class="blog_menu">
        <ul>
            <li><a href="blog.php">View All Posts</a></li>
            <li><a href="insert_blog.php">Create Your own Blog post</a></li>
            <li><a href="view_user_posts.php">View all your Posts</a></li>
            <form method="GET" action="search_results.php">
    <input type="text" name="query" placeholder="Search...">
    <button type="submit">Search <i class='bx bx-search-alt' ></i></button>
</form>
        </ul>
    </nav>
    <h1 class="blog_introduction">The Blog, view and edit your posts.</h1>
    <div class="post-container">
    <p class="all_posts_title">Edit your blog post</p>
        <?php
       if(isset($_GET['id'])) {
        $postId = $_GET['id'];
        $postData =  fetchBlogPostByPostId($postId);
        $postId = $postData['post_id'];
        $postTitle = $postData['title'];
        $postContent = $postData['content'];
        $postImagePath = $postData['picture_path'];
        $picPath = getExistingPicturePath($postId);
    }
    if (isset($_POST['submit'])) {
        // Check if a new file was selected
        if ($_FILES['blogPicture']['error'] !== UPLOAD_ERR_NO_FILE) {
            // Process picture upload and move it to the server
            $pictureName = $_FILES['blogPicture']['name'];
            $pictureTemp = $_FILES['blogPicture']['tmp_name'];
            
            $uploadDir = 'blog_pictures/'; // Directory to store uploaded pictures
            $picturePath = $uploadDir . $pictureName;
            
            move_uploaded_file($pictureTemp, $picturePath);
        } else {
            // No new file selected, retain existing picture path
            $picturePath = $postImagePath;
        }
    
        // Update the database
        editBlogPost($_POST['title'], $_POST['content'], $picturePath, $postId);
        echo "<p class='message'>Blog Post edited successfully <a href='view_edit_post.php?id={$postId}'>Go Back</a></p>";
    }
    if(isset($_POST['deletePost'])) {
        deleteBlogPost($postId);
        echo "<p class='message'>Blog Post deleted successfully <a href='view_user_posts.php'>Go Back</a></p>";

    }    

        ?>
       <form action="" method="post" enctype="multipart/form-data">
    <label for="blogPicture">Blog Picture, insert your own or it will have a standard one selected by Us.</label>
    <input type="file" name="blogPicture" id="blogPicture">
    <div id="imagePreview">
    <?php if (!empty($postImagePath)) : ?>
            <img src="<?php echo $postImagePath; ?>" alt="Blog Picture">
        <?php endif; ?>
    </div>
    <label for="title">Blog Title :</label>
    <input type="text" name="title" id="title" value="<?php if(!empty($postTitle)) echo $postTitle; ?>">
    <label for="content">Content :</label>
    <textarea name="content" id="content"><?php if(!empty($postContent)) echo $postContent; ?></textarea>
    <input type="submit" name='submit' value="Edit Post" class="editButton">
    <input type='submit' value='Delete' name='deletePost' class="editButton">
</form>
    </div>
    <?php 
    include "inc/blog_sidebar.php";
    ?>
</section>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        const blogPictureInput = document.getElementById('blogPicture');
        const imagePreview = document.getElementById('imagePreview');
        
        blogPictureInput.addEventListener('change', function(event) {
            const file = event.target.files[0];
            if (file) {
                const reader = new FileReader();
                reader.onload = function(e) {
                    const image = document.createElement('img');
                    image.src = e.target.result;
                    imagePreview.innerHTML = '';
                    imagePreview.appendChild(image);
                };
                reader.readAsDataURL(file);
            } else {
                imagePreview.innerHTML = '';
            }
        });
    });
</script>





