<?php
include "inc/headerblank.php"; // Replace this with the actual file that contains your database connection
?>
<head>
    <link rel="stylesheet" href="css/dashboard.css">
    <style>
        #nav-logo ul li:nth-child(4) {
            border-bottom: 4px solid white;
            border-radius: 8px;
        }
        #nav-logo ul li:nth-child(1) {
            border-bottom: unset;
            border-radius: unset;
        }
        .message p {
            text-align: center;
        }
    </style>
</head>

<?php

if(isset($_GET['id'])) {
    $docid = $_GET['id'];
    $documentData= getSpecificDocument($docid);
    if(!$documentData) {
        echo "<section class='file-progress'>";
        echo "<h1 class='progress-title'>Viewing Project NR.$docid</h1>";
            echo "<p class='pending'>Project is still pending</p>"; 
            echo "<p>Your Project has yet to be seen or completed by the translator.</p>"; 
            echo "</section>";  
         } else {
            $status = $documentData['status'];
            if ($status == 2) {
                echo "<section class='file-progress'>";
                echo "<h1 class='progress-title'>Viewing Project NR.$docid</h1>";
                    echo "<p class='pending'>You have accepted the Project</p>"; 
                    echo "<p>Thank you for choosing G Translationsm we look forward to working with you again in the future.</p>"; 
                    echo "</section>"; 
            } else {
$userId = $_SESSION['userid'];
$resend = fetchDocumentData($userId);
$userName = $documentData['name'] . ' ' . $documentData['surname'];


        $file = $documentData['finished_doc'];
        $submission_date = $documentData['submission_date'];
        $details = $documentData['document_details'];
        $requestId = $documentData['documentid'];

        echo "<section class='file-progress'>";
        echo "<h1 class='progress-title'>Viewing Project NR.$requestId</h1>";

            echo "<p>Request Details: <span>$details</span></p>";
            echo "<p>Submission Date: <span>$submission_date</span></p>";
            if ($file) {
                $filename = basename($file);
                $filePath = "submits/" . $filename;
                echo "<a href='$filePath' download>Download Document</a>";
            } else {
                echo "Document not available.";
            }
            if(isset($_POST['accept'])) {
                acceptSubmitedDoc($docid);
                echo "<div class='message' id='closePopUp'><p>Document submitted successfully!</p>";
            echo "<button class='closeMesagge'>Close</button>";
            echo "</div>";  
            } else if(isset($_POST['decline'])) {
                header("Location: modify_document.php?id=$docid");
                declineDocument($docid);
            } 
            ?>
            <form action="" method="post">
                <label>Please review your document and choose if you want to accept it or decline it. If you choose to decline, the document will be re-sent to the translator for further improvements. All the best, G Translations</label>
                <input type="submit" name="decline" id="decline" value="Decline">
                <input type="submit" name="accept" id="accept" value="Accept">
            </form>
            <?php
        }

        echo "</section>";
    }
}
?>
<script>
     function downloadDocument(filename) {

        window.location.href = 'download_document.php?filename=' + encodeURIComponent(filename);
    }
</script>