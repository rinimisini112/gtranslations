<?php 
include "inc/headerblank.php";
?>
<head>
    <link rel="stylesheet" href="css/workform.css">
    <style>
        #nav-logo ul li:nth-child(3) {
            border-bottom: 4px solid white;
            border-radius: 8px;
        }
        #nav-logo ul li:nth-child(4) {
            border-bottom: unset;
            border-radius: unset;
        }
    </style>
</head>

<?php
$userid = $_SESSION['userid'];
$documentRequests = fetchAllDocumentData($userid); // Assuming you have a function to fetch all document requests
?>

<section class="file-progress">
    <h1 class='progress-title'>View Progress and Current Updates About Your Projects - Documents.</h1>

    <?php if (!empty($documentRequests)): ?>
        <!-- Show table with document requests -->
        <table>
            <thead>
                <tr>
                    <th>Request ID</th>
                    <th>Language</th>
                    <th>Details</th>
                    <th>Translator</th>
                    <th>Deadline</th>
                    <th>View</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($documentRequests as $request): ?>
                    <tr>
                    <td><?php echo isset($request['documentid']) ? $request['documentid'] : ''; ?></td>
                        <td><?php echo $request['language']; ?></td>
                        <td><?php
                        $details = $request['request_details'];
                        if (strlen($details) > 25) {
                            echo substr($details, 0, 25) . '...';
                        } else {
                            echo $details;
                        }
                         ?></td>
                        <td><?php echo $request['name'] . ' ' . $request['surname']; ?></td>
                        <td><?php echo $request['deadline']; ?></td>
                        <td>
                        <?php if ($request['status'] == 2): ?>
                            Finished
                        <?php elseif ($request['status'] == 1): ?>
                            <a href='workform_single_doc.php?id=<?php echo $request['documentid']; ?>'>Declined, Priority!</a>
                            <?php else: ?>
                            <a href='workform_single_doc.php?id=<?php echo $request['documentid']; ?>'>View Details</a>
                        <?php endif; ?>
                    </td>                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <!-- Show message when no document requests are available -->
        <p>No document requests available.</p>
    <?php endif; ?>
</section>