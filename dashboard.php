<?php 
include "inc/headerblank.php";
?>
<head>
    <link rel="stylesheet" href="css/dashboard.css">
</head>

<?php
$userid = $_SESSION['userid'];
$doc_details = fetchDocumentTranslator($_SESSION['translatorid']);
if($doc_details) {
$status = $doc_details['status']; }
?>

<section class="doc-progress">
    <h1 class="progress-title">View Current Projects</h1>
    <table>
        <thead>
            <tr>
            <th>Status</th>                
                <th>User Name</th>
                <th>Request Details</th>
                <th>Deadline</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php   
            $translatingRequests = getTranslatingRequestsForTranslator($_SESSION['translatorid']);

            foreach ($translatingRequests as $request) {
                $userName = $request['name'] . ' ' . $request['surname']; 
                $requestDetails = $request['request_details'];
                $deadline = $request['deadline'];
                $projectId = $request['documentid'];
                $docUserId = $request['userid'];
                $_SESSION['docId'] = $docUserId;

                if(strlen($requestDetails)>60){
                    $requestDetails=substr($requestDetails,0,50) . " ...";}
            ?>
            <tr>
                <?php if($request['status'] == 2) {
                    echo "<td>Aceppted</td>";
                 } else if($request['status'] == 1) {
                        echo "<td class='white'>Re sent!</td>";
                } else if ($request['status']== 0){
                    echo "<td>New!</td>";
                    } ?>
                <td><?php echo $userName; ?></td>
                <td><?php echo $requestDetails; ?></td>
                <td><?php echo $deadline; ?></td>
                <td>
                        <?php if ($request['status'] == 2): ?>
                            Finished
                        <?php elseif ($request['status'] == 1): ?>
                            <a href='view_request.php?id=<?php echo $request['documentid']; ?>'>Declined,Priority!</a>
                            <?php else: ?>
                            <a href='view_request.php?id=<?php echo $request['documentid']; ?>'>View Details</a>
                        <?php endif; ?>
                    </td>               </tr>
            <?php
            }
            ?>
        </tbody>
    </table>
</section>
</section>