<?php 
include "inc/headerblank.php";
?>
<head>
    <link rel="stylesheet" href="css/workform.css">
    <style>
        #nav-logo ul li:nth-child(3) {
            border-bottom: 4px solid white;
            border-radius: 8px;
        }
        #nav-logo ul li:nth-child(4) {
            border-bottom: unset;
            border-radius: unset;
        }
    </style>
</head>

<?php
if (isset($_GET['id'])) {
    $documentId = $_GET['id'];

    // Fetch progress information for the specified document
    $documentData = fetchDocumentDataById($documentId);

    // Check if progress data is available
    if ($documentData) {
        $requestId = $documentData['documentid'];
        $languageName = $documentData['language'];
        $docDetails = $documentData['request_details'];
        $translatorName = $documentData['name'] . ' ' . $documentData['surname'];
        $deadline = $documentData['deadline'];
    }
}
?>

<section class="file-progress">
    <h1 class='progress-title'>View Progress and current updates about your project - Document.</h1>

    <?php if (isset($documentData)): ?>
        <!-- Show progress information -->
        <p class='request-id'>Your document ID is: <?php echo $requestId ?> </p>
        <p class='language'>You selected: <?php echo $languageName ?></p>
        <p class='document-details'>Details: <?php echo $docDetails ?></p>
        <p class='document-status'>Your document has been assigned to Translator: <?php echo $translatorName ?></p>
        <p class='document-finish-date'>Your document is set to be finished approximately on: <?php echo $deadline ?></p>
        <a class="modifyButton" href="workformmodify.php?id=<?php echo $documentId?>">Modify</a>
    <?php else: ?>
        <!-- Show message when no progress information is available -->
        <p>No progress information available for the specified document.</p>
    <?php endif; ?>
</section>