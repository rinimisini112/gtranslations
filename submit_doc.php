<?php 
include "inc/headerblank.php";
?>
<head>
    <link rel="stylesheet" href="css/dashboard.css">
    <style>
        #nav-logo ul li:nth-child(2) {
    border-bottom: 4px solid white;
    border-radius: 8px;
  }
  #nav-logo ul li:first-child {
    border-bottom:   unset;
    border-radius: unset;
  }
    </style>
</head>
<?php 
?>
    
<section class="file-form">
    <h1 class="file-title">Submit Users Document.</h1>
<form method="post" enctype="multipart/form-data" id="document">
    
<label for="file">Select your document</label>
    <input type="file" name="file" id="file" required title="Please Upload your file">
        <label for="username">Client name</label>
        <select name="username" id="username">
            <option value="0">Choose</option>
            <?php
        $translatorId = $_SESSION['translatorid'];
        $users = getUsersForTranslator($translatorId);

        foreach ($users as $user) {
            $userId = $user['userid'];
            $userName = $user['name'] . ' ' . $user['surname'];
            $documentIds = explode(',', $user['documentid']);

            // Loop through document IDs and create separate options
            foreach ($documentIds as $documentId) {
                echo "<option value='$userId:$documentId'>$userName - DocNr.$documentId</option>";
            }
        }
        ?>
</select>
        </select>
        <label for="details">Extra details u wish to add?</label>
        <textarea name="details" id="details"></textarea>
         <input type="submit" name="submitFile" value="Submit">
    </form>
    <p class="track-progress-text">Track your submited documents<a href="submited_doc.php">HERE!</a></p>
    <?php
 if (isset($_POST['submitFile'])) {
    $selectedOption = $_POST['username'];

    list($clientId, $documentId) = explode(':', $selectedOption);
    $userId = $_SESSION['userid'];
    $translatorData = getTranslatorDataByUserId($userId);
    $translatorId = $translatorData['translatorid'];
    $languageId = $translatorData['languageid'];
    $details = $_POST['details'];
    $submissionDate = date("Y-m-d"); 
    $uploadedFilePath = handleSubmitUpload('file');
        
        // Now, you can call the insertTranslatingRequest function with the selected translator ID
        if ($uploadedFilePath && insertTranslatedDocument($documentId, $clientId, $languageId, $details, $uploadedFilePath, $translatorId)) {
            echo "<div class='message' id='closePopUp'><p>Document submitted successfully!</p>";
            echo "<button class='closeMesagge'>Close</button>";
            echo "</div>";  
        } else {
            echo "Failed to submit translating request. Please try again.";
        }
    }


    ?>
</section>
<script>

$(document).ready(function () {
          $.validator.addMethod("alphabetsOnly", function(value, element) {
      return /^[a-zA-Z\s]+$/.test(value);
    }, "Enter letters only.");


          // Add validation rules and messages to your registration form
          $.validator.addMethod("valueNotEquals", function(value, element, arg){
  return arg !== value;
 }, "Value must not equal arg.");
        $('#document').validate({
            rules: {
                username: { 
                valueNotEquals: "0",
                },
                details: {
                    required: true,
                },
            },
            messages: {
                username: {
                    valueNotEquals: "Please select a Client",
                },
                details: {
                    required: "Please enter details for the customer to see!",
                },
              
        }});
    })
</script>