<?php
include "inc/header.php";
?>

<head>
    <link rel="stylesheet" href="css/packages.css">
    <style>
        .sidebar .first_menu li:nth-child(2) a  {
            border-bottom: 2px solid white;
        }
        #footer-section {
    border-top:4px solid rgba(255, 255, 255, 0.608) ;   
     }
    </style>
</head>
<section class="services-wrapper">
    <p class="service-big-title" id="slide-title">
        What we have to offer!
    </p>
    <div class="service-info-wrapper">
        <div class="service-info-item">
            <span class="service-info-number">001</span>
            <p class="service-info-title">Professional Translation Services</p>
            <p class="service-info-text">Mention that your office offers professional translation services for various language pairs, covering a wide range of industries and sectors.</p>
            <a href="login-index.php"><i class='bx bxs-chevrons-right'></i></a>
        </div>
        <div class="service-info-item">
            <span class="service-info-number">002</span>
            <p class="service-info-title">Document Translation</p>
            <p class="service-info-text">Highlight your ability to translate documents of different types, such as legal documents, certificates, academic transcripts, business contracts, medical reports, etc..</p>
            <a href="login-index.php"><i class='bx bxs-chevrons-right'></i></a>
        </div>
        <div class="service-info-item">
            <span class="service-info-number">003</span>
            <p class="service-info-title">Website Translation</p>
            <p class="service-info-text">Mention that your office offers professional translation services for various language pairs, covering a wide range of industries and sectors.</p>
            <a href="login-index.php"><i class='bx bxs-chevrons-right'></i></a>
        </div>
        <div class="service-info-item">
            <span class="service-info-number">004</span>
            <p class="service-info-title">Localization Services</p>
            <p class="service-info-text">Mention that your office offers professional translation services for various language pairs, covering a wide range of industries and sectors.</p>
            <a href="login-index.php"><i class='bx bxs-chevrons-right'></i></a>
        </div>
        <div class="service-info-item">
            <span class="service-info-number">005</span>
            <p class="service-info-title">Interpretation Services</p>
            <p class="service-info-text">Mention that your office offers professional translation services for various language pairs, covering a wide range of industries and sectors.</p>
            <a href="login-index.php"><i class='bx bxs-chevrons-right'></i></a>
        </div>
        <div class="service-info-item">
            <span class="service-info-number">006</span>
            <p class="service-info-title">Confidentiality and Data Security</p>
            <p class="service-info-text">Mention that your office offers professional translation services for various language pairs, covering a wide range of industries and sectors.</p>
            <a href="login-index.php"><i class='bx bxs-chevrons-right'></i></a>
        </div>
    </div>
    <?php
    ?>
    <div class="package-plan-wrapper">
        <p class="package-plan-intro"><span class="first-row">Great Deals</span><span class='even-row'>,even</span><span class="second-row">Greater Service</span></p>
        <div class="package-plan-item">
            <span class="package-plan-side-text">Basic</span>
            <p class="monthly-yearly">Monthly and Yearly Subscribtions - Access to Blog and Newsletter</p>
            <ul class="package-plan-features">
                <li class="package-plan-deal">
                    Basic Translating Services :
                    <span>
                        Get professional translation services for documents and content in various languages and industries.
                    </span>
                </li>
                <li class="package-plan-deal">
                    Multi-User Collaboration :
                    <span>
                        Collaborate seamlessly with your team members or clients on translation projects in real-time.
                    </span>
                </li>
                <li class="package-plan-deal">
                    Extended File Size Limit :
                    <span>
                        Upload files up to 10 GB of space, allowing you to handle large and complex translation projects.
                    </span>
                </li>
                <li class="package-plan-deal">
                    24/7 Support :
                    <span>
                        Enjoy round-the-clock customer support to address any queries or issues you may have.
                    </span>
                </li>
            </ul>
            <p class="login-now">Log in now, or register to view pricing</p>
        <a href="<?php echo "login-index.php?redirect=packages.php"; ?>">
            <button type="button" class="login-to-view">Login to view Prices</button>
        </a>
</div>
        <div class="package-plan-item">
            <span class="package-plan-side-text">Master</span>
            <p class="monthly-yearly">Monthly and Yearly Subscribtions - Access to Blog and Newsletter</p>
            <ul class="package-plan-features">
                <li class="package-plan-deal">
                Enhanced Translating Capabilities :<span> Access to advanced translation tools and technologies to deliver precise and culturally sensitive translations.</span>
                </li>
                <li class="package-plan-deal">
                Multi-User Collaboration :<span> Collaborate seamlessly with your team or clients on translation projects, facilitating efficient teamwork.</span>
                </li>
                <li class="package-plan-deal">
                Extended File Size Limit: 6GB<span> Upload and handle files up to 6GB in size, enabling you to manage even more substantial translation tasks.</span>
                </li>
                <li class="package-plan-deal">
                Priority Support :<span> Receive priority assistance from our dedicated support team, ensuring prompt and reliable service.</span>
                </li>
            </ul>
            <p class="login-now">Log in now, or register to view pricing</p>           
            <a href="<?php echo "login-index.php?redirect=packages.php"; ?>">
    <button type="button" class="login-to-view">Login to view Prices</button>
</a>
        </div>
        <div class="package-plan-item">
            <span class="package-plan-side-text">Proffesional</span>
            <p class="monthly-yearly">Only Yearly Subscribtions - Post in our blog  </p>
            <ul class="package-plan-features">
                <li class="package-plan-deal">
                Cutting-Edge Translating Tools :<span> Access to the latest and most advanced translation software and technologies to ensure accurate and efficient translations.</span>
                </li>
                <li class="package-plan-deal">
                Unlimited User Collaboration :<span> Collaborate seamlessly with your team members, clients, or partners on translation projects in real-time.</span>
                </li>
                <li class="package-plan-deal">
                Unrestricted File Size Limit :<span> Upload and handle files of any size, even large and complex documents, for translation without any restrictions.</span>
                </li>
                <li class="package-plan-deal">
                VIP Support :<span> Enjoy priority access to our 24/7 customer support team for any assistance or queries you may have.</span>
                </li>
            </ul>
<p class="login-now">Log in now, or register to view pricing</p>          
<a href="<?php echo "login-index.php?redirect=packages.php"; ?>">
    <button type="button" class="login-to-view">Login to view Prices</button>
</a>
        </div>
    </div>
</section>
<script>
     gsap.registerPlugin(ScrollTrigger);
     function showTitle(title) {
        gsap.from(title, {
            opacity:0,
            duration:3,
            ease: "power2.out",
            scrollTrigger: {
                trigger : title,
                start:"top 50%",
                end: "top 50%",
                toggleActions: "restart none play play"
            }
        });}
        const planTitle = document.querySelector('.package-plan-intro');
        showTitle(planTitle);
    function fadeFromRight(title) {
        gsap.from(title, {
            x:-1000,
            opacity:0,
            duration:1,
            ease: "power2.out",
            scrollTrigger: {
                trigger : title,
                start:"top 50%",
                end: "top 50%",
                toggleActions: "restart none play play"
            }
        });}
    function fadeInLeft(elements) {
    gsap.from(elements, {
        x: 300,
        opacity: 0,
        duration: 2,
        ease: "power2.out",
        scrollTrigger: {
            trigger: ".service-info-wrapper", // Use the first element in the group as the trigger
            start: "top 70%", // Start the animation when the top of the element is 80% in view
            end: "top 70%", // End the animation when the bottom of the element reaches the bottom of the viewport
            toggleActions: "restart none play play" // Restart the animation when the start point is reached, play when scrolling down
        }
    });} 
    function fadeServiceBoxes(elements) {
    gsap.from(elements, {
        scale: 0.5, // Start at 50% scale
        opacity: 0,
        duration: 2,
        ease: "power2.out",
        scrollTrigger: {
            trigger: elements, // Use the first element in the group as the trigger
            start: "top 90%", // Start the animation when the top of the element is 80% in view
            end: "top 90%", // End the animation when the bottom of the element reaches the bottom of the viewport
            toggleActions: "restart none play play" // Restart the animation when the start point is reached, play when scrolling down
        }
    });}    
    const serviceBoxesAnimation = document.querySelectorAll('.package-plan-item');
    fadeServiceBoxes(serviceBoxesAnimation);
    const fadeTitleFromRight = document.getElementById('slide-title');
    fadeFromRight(fadeTitleFromRight);
    const fadeInLeftElements = document.querySelectorAll(".service-info-item");
    fadeInLeft(fadeInLeftElements);
</script>
<?php
include "inc/footer.php";
?>