 <?php   
 include "functions.php";
 ?>
  <!DOCTYPE html>
  <html lang="en">
    <head>
      <meta charset="UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <title>Document</title>
      <link rel="stylesheet" href="css/registerform.css" />
      <script src="java-jquery/jquery.js"></script>
      <script src="java-jquery/jquery-ui.js"></script>
      <script src="java-jquery/jquery.validate.js"></script>
      <script src="java-jquery/jquery.validate.min.js"></script>
      <link
        href="https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css"
        rel="stylesheet"
      />
      <style>
        @import url("https://fonts.googleapis.com/css2?family=Oswald:wght@600&family=Playfair+Display:wght@400;600;700&family=Roboto:wght@300;500;700&family=Source+Sans+3:wght@300&family=Ysabeau+SC:wght@300;600&display=swap");
      </style>
    </head>
    <body>
      <header id="nav-logo">
        <p class="title">G Translations<span>.</span></p>
        <div class="mini-nav">
        <a class="home" href='index.php'>Home</a>
          <button type="button" class="header-button-register" id="login-register">
            <a href="login-index.php">Log in</a>
          </button>
          </div>
      </header>
      <header id="header-container2">
        <div id="frame-login">
            <form id="register-form" method="POST">
               <h1 class="register-title">Sign up Into your Account!</h1>
               <?php
            if(isset($_POST['register'])) {
              register($_POST['name'],$_POST['surname'],$_POST['birthdate'],$_POST['personalnr'],
              $_POST['phone'],$_POST['email'],$_POST['password']);
            }
            ?>
               <div class="input-box">
                <div class="first-row">
                <div class="form-row">
                  <label for="name">Name</label>
                  <input type="text" name="name" />
                  </div>
                  <div class="form-row">
                  <label for="surname">Surname</label>
                  <input type="text" name="surname" />
               </div>
                <div class="form-row">
                  <label for="birthdate">Date Of Birth</label>
                  <input type="date" name="birthdate" />
               </div>
                <div class="form-row">
                <label for="personalnr">Personal Number</label>
                <input type="tel" name="personalnr" id="personalnr" />
          </div>
          </div>
          <div class="second-row">
          <div class="form-row">
                <label for="phone">Telephone Number</label>
                <input type="tel" name="phone" id="phone" />
                </div>
                <div class="form-row">
                  <label for="email">Email</label>
                  <input type="email" name="email" id="" />
                </div>
                <div class="form-row">
                <label for="password">Password</label>
                <input type="text" name="password" /> 
          </div>
          <input type="submit" value="Register" class="register-btn" name="register"/>
          </div>
          </div>
            <span>
              Already Have an Account?
              <a href="login-index.php"> Login</a></span>
              </form>
            </header>
      <script>
        $(document).ready(function () {
          $.validator.addMethod("alphabetsOnly", function(value, element) {
      return /^[a-zA-Z\s]+$/.test(value);
    }, "Enter letters only.");


          // Add validation rules and messages to your registration form
          $("#register-form").validate({
            rules: {
              name: {
                required: true,
                minlength:2,
                alphabetsOnly: true,
              },
              surname: {
                required: true,
                minlength:2,
                alphabetsOnly: true,
              },
              birthdate: {
                required: true,
              },
              email: {
                required: true,
                email: true,
              },
              phone: {
                required: true,
                number: true,
                maxlength:15,
              },
              personalnr: {
                required: true,
                number: true,
              },
              password: {
                minlength: 6,
                maxlength: 15,
                required: true,
              },
            },
            messages: {
              name: {
                required: "Please enter your name",
              },
              surname: {
                required: "Please enter your last name",
              },
              birthdate: {
                required: "Please enter your date of birth",
              },
              email: {
                required: "Please enter your email",
                email: "Enter a valid email address",
              },
              phone: {
                required: "Enter your telephone number",
                number: "Enter a valid telephone number",
              },
              personalnr: {
                required: "Please enter your personal number",
                number: "Please enter a valid personal number",
              },
              password: {
                required: "Please enter your password",
              },
            },
            errorPlacement: function (error, element) {
                // Place the error message below the input element
                error.insertAfter(element);
            }
        });
          });
          
      </script>
    </body>
  </html>
