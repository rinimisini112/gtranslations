<?php include "inc/headerblank.php"; // Replace this with the actual file that contains your database connection
?>
<head>
    <link rel="stylesheet" href="css/dashboard.css">
    <style>
        #nav-logo ul li:nth-child(4) {
    border-bottom: 4px solid white;
    border-radius: 8px;
  }
  #nav-logo ul li:nth-child(1) {
    border-bottom:   unset;
    border-radius: unset;
  }
    </style>
</head>
<section class='file-modify'>
<?php
if(isset($_GET['id'])) {
    $requestId = $_GET['id'];

$userId = $_SESSION['userid'];
$request = getSubmitedDocForUser($userId);
$viewTranslatingRequest = fetchDocumentData($requestId);

    $details = $viewTranslatingRequest['request_details'];
    $deadline = $viewTranslatingRequest['deadline'];
    $requestId = $viewTranslatingRequest['documentid'];
    $userName = $request['name'] . ' ' . $request['surname'];
    if(isset($_POST['resend'])) {
        resendTranslatingRequest($requestId,$_POST['details'],$_POST['deadline']);
        echo "<div class='message' id='closePopUp'><p>Request was re sent successfully!</p>";
        echo "<button class='closeMesagge'><a href='workformprogress.php'>View.</a></button>";
        echo "</div>";
        }
}

?>

    <h1 class='progress-title'>Re Sending Project NR.<?php echo $requestId?></h1>

    
    <p class="change-info">Hi <?php echo $userName?>, in the areas below enter what details would u like changed about the document and a new deadline if u have one if not the same deadline u set before will remain.<span></p>
            <form action="" method="post" id="reSend">
    <label>Enter Details :</label>
    <textarea name="details" id="details"><?php if(!empty($details)) echo $details?></textarea>
    <label>New Deadline? : </label>
    <input type="datetime-local" name="deadline" id="deadline" value="<?php if(!empty($deadline)) echo $deadline ?>">


            <label>Tell your translator what would you like to be changed before sending again. All the best, G Translations</label>
            <input type="submit" name="resend" id="accept" value="Send Again">
        </form>
    </section>
<script>

</script>