<?php 
include "inc/headerblank.php";
?>
<head>
    <link rel="stylesheet" href="css/admin.css">
    <style>
        .sidebar .first_menu li:nth-child(1) a  {
            border-bottom: 2px solid white;
        }
        body {
            overflow-y: unset;
        }
        .translator_wrapper {
            justify-content: right;
            position: relative;
        }
        .message {
          text-align: center;
        }

    </style> 
</head>
<nav class="staff_nav">
<p class="staff_title">Two step - registration process.</p>
<a href="staff.php">Go Back</a>
</nav>
<div class="translator_wrapper">
    <?php 
    if(isset($_GET['id'])) {
        $userId = $_GET['id'];
        $translatorData = getTranslatorData($userId);
        $transName = $translatorData['name'];
        $transSurname = $translatorData['surname'];
        $transPhone = $translatorData['phone'];
        $transEmail = $translatorData['email'];

    }
    ?>
    <p class="process_info">Step 2 - Complete Translator registration.</p>
<div class="user-details">
    <form method="POST" id="register_translator">
        <div class="form-grid">
            <div class="form-col">
                <div class="form-row">
                    <label for="name">Name :</label>
                    <input type="text" name="name" value="<?php if(!empty($transName)) echo $transName; ?>"/>
                </div>
                <div class="form-row">
                    <label for="surname">Surname :</label>
                    <input type="text" name="surname" value="<?php if(!empty($transSurname)) echo $transSurname; ?>"/>
                </div>
                <div class="form-row">
                    <label for="phone">Phone Number :</label>
                    <input type="tel" name="phone" id="phone" value="<?php if(!empty($transPhone)) echo $transPhone; ?>"/>
                </div>
            </div>
            <div class="form-col">
                <div class="form-row">
                    <label for="email">Email :</label>
                    <input type="email" name="email" id="" value="<?php if(!empty($transEmail)) echo $transEmail; ?>"/>
                </div>
                <div class="form-row">
                    <label for="language">Select Language :</label>
                    <select name="language" id="language">
                        <option value="Select">Select</option>
                        <?php
                        $languagesResult = getAllLanguages();
                        while ($row = mysqli_fetch_assoc($languagesResult)) {
                            echo '<option value="' . $row['languageid'] . '">' . $row['language'] . '</option>';
                        }
                        ?>
                </select>
                </div>
                <input type="submit" name="finish" value="Finish" id="save">
            </div>
            
        </div>
    </form>
    <?php 
            if(isset($_POST['finish'])) {
                $staffName = $_POST['name'];
                $staffSurname = $_POST['surname'];
                $staffPhone = $_POST['phone'];
                $staffEmail = $_POST['email'];
                $language = $_POST['language'];
                
                insertTranslator($staffName,
                $staffSurname,$staffPhone,
                $staffEmail,$language,$userId);
                         echo "<div class='message' id='closePopUp'><p>Step 2 completed. Translator sucessfully registered!</p>";
              echo "<button class='closeMesagge'><a href='staff.php'>Continue!</a></button>";
              echo "</div>";
               }
            ?>
</div>
    </div>
    <script>
        $(document).ready(function () {
          $.validator.addMethod("alphabetsOnly", function(value, element) {
      return /^[a-zA-Z\s]+$/.test(value);
    }, "Enter letters only.");


          // Add validation rules and messages to your registration form
          $("#register_translator").validate({
            rules: {
              name: {
                required: true,
                minlength:2,
                alphabetsOnly: true,
              },
              surname: {
                required: true,
                minlength:2,
                alphabetsOnly: true,
              },
              email: {
                required: true,
                email: true,
              },
              phone: {
                required: true,
                number: true,
                maxlength:15,
              },
            },
            messages: {
              name: {
                required: "Please enter a Name",
              },
              surname: {
                required: "Please enter a Surname",
              },
              email: {
                required: "Please enter an email address",
                email: "Enter a valid email address",
              },
              phone: {
                required: "Enter an telephone number",
                number: "Enter a valid telephone number",
              },
            },
            errorPlacement: function (error, element) {
                // Place the error message below the input element
                error.insertAfter(element);
            }
        });
          });
    </script>