<?php 
include "inc/headerblank.php";
?>
<head>
    <link rel="stylesheet" href="css/mysubscriptions.css">
</head>
<section class="myaccount">
<p class="sidebar-title">My Account</p>
<ul class="user-misc">

    <?php
    generateProfileNavigation();
    ?>

</ul>
</section>
<section class="accountinfo">
    <div class="cancel-plan-row">
        <?php
          $userid = $_SESSION['userid'];
          $planData = getPlanDetails();
          $subType = isset($planData['subscription_type']) ? $planData['subscription_type'] : null;
          $applyDate = isset($planData['application_date']) ? $planData['application_date'] : null;
          $expDate = isset($planData['expiration_date']) ? $planData['expiration_date'] : null;
          $planName = isset($planData['plan_name']) ? $planData['plan_name'] : null;
          
        ?>
    <p class="detail-title">My Subscriptions</p>
    <?php  
    $userid = $_SESSION['userid'];
    $planData = getPlanDetails();
    $subType = isset($planData['subscription_type']) ? $planData['subscription_type'] : null;
    $applyDate = isset($planData['application_date']) ? $planData['application_date'] : null;
    $expDate = isset($planData['expiration_date']) ? $planData['expiration_date'] : null;
    $planName = isset($planData['plan_name']) ? $planData['plan_name'] : null;
    $isSubscribed = !empty($subType) && !empty($applyDate) && !empty($expDate) && !empty($planName);
   
    

    

    if (isset($_POST['cancel'])) {
        $checkQuery = "SELECT * FROM clients WHERE userid = $userid";
        $checkResult = mysqli_query($dbconn, $checkQuery);
        if (mysqli_num_rows($checkResult) === 0) {
            echo "<div class='buy-title'><p>You are not subscribed to any plan.</p><i class='bx bx-x'></i></div>";
        } else {
        // Display the confirmation form
        echo "<div class='buy-title'>";
        echo "<form id='form2' method='post' action='" . $_SERVER['PHP_SELF'] . "'>";
        echo "<p>Are you sure you want to unsubscribe? <input type='submit' name='confirm' id='confirm' value='Confirm'><i class='bx bx-x'></i></p>";
        echo "<input type='hidden' name='userid' value='$userid'>";
        echo "</form>";
        echo "</div>";
    }
}
    if (isset($_POST['confirm'])) {
        // If the confirmation form is submitted, delete the plan and redirect to mysubscription.php
        deleteClientsPlan($_POST['userid']);
        header("Location: mysubscription.php");
    }

   ?>
            <form id='form1' action="" method="post">
                <input type="hidden" name="userid" value="$userid">
                <input type="submit" name='cancel' id='cancel' value="Cancellation">
            </form>
            </div>
            <div class="user-plan-info">
                <div class="card-acc-status-container">
                <div class="first-box">
                    <p class="payment-method">Payment Method</p>
                    <?php  
                    if ($subType && $applyDate && $expDate && $planName) {
                        echo "<p class='sub-details'>mastercard(*** 1668) exp 5/2026</p>";
                    } else {
                        echo "<p class='sub-details'>None</p>";
                    }
                    ?>
                </div>
                <div class="second-box">
                <p class="payment-method">Payment Status</p>
                <p class="sub-details">Subscribtion Status: <?php 
                if ($subType && $applyDate && $expDate && $planName) {
                    echo "<span class='active'>Active</span>";
                } else {
                    echo "<span class='not-active'>Not Active</span>";
                }
                ?></p>                    
                </div>
                </div>
                <div class="subscription-info">
                <p class="details-title">Subscription Details</p>
                <div class="sub-info">
                <p class="plan-tier">Current Subscribtion</p>
                <p class="plan-plan"><?php if($planName)
                { echo $planName;
                } else 
                 {echo "Not Subscribed";
                }
                 ?></p>
                <p class="apply-date">Purchase date : <?php echo $applyDate; ?></p>
                <p class="expiration-date">Expiration Date : <?php echo $expDate; ?></p>
                <p>Subscribtion Type</p>
                <p class="plan-type"><?php echo $subType?></p>
                </div>
                </div>
            </div>
</section>
</header>
<script>
    $(document).ready(function() {

        var closePopUp = $('.bx-x');
  
        closePopUp.on('click', function() {

    $('.buy-title').fadeOut();
  });
});
</script>