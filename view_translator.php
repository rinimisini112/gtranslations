<?php 
include "inc/headerblank.php";
?>
<head>
    <link rel="stylesheet" href="css/admin.css">
    <style>
        .sidebar .first_menu li:nth-child(1) a  {
            border-bottom: 2px solid white;
        }
        body {
            overflow-y: unset;
        }
        .staff_nav form input {
            width: 20%;
            height: 3.5;
            color: white;
            font-size: 2.2rem;
            position: absolute;
            top: 0;
            right: 0;
            border: none;
            background-color: #111;
            display: flex;
            justify-content: center;
            align-items: center;
            transition: 0.15s ease;
            cursor: pointer;
            }
            .staff_nav form input:hover {
            background-color: #111;
            text-shadow: 0 0 3px white;
            border-left: 1px solid white;
            }
    </style>
</head>
<nav class="staff_nav">
<p class="staff_title">View staff, Delete employees.</p>
<form action="" method="post">
<input type="submit" name="confirmDelete" value="Delete">
</form>
</nav>
<div class="translator_wrapper">
<?php
if(isset($_GET['id'])) {
    $staffId = $_GET['id'];
    $staffData = fetchTranslatorForAdmin($staffId);
    if(isset($_POST['confirm'])) {
        deleteTranslator($staffId);
 }
    if (isset($_POST['confirmDelete'])) {
        echo "<div class='buy-title'>";
        echo "<form method='post' action='" . $_SERVER['PHP_SELF'] . "?id=$staffId'>"; // Pass the staffId in the URL
        echo "<p>Confirm Translator Deletion<input type='submit' name='confirm' id='switch' value='Confirm'><i class='bx bx-x'></i></p>";
        echo "</form>";
        echo "</div>";
    }

   
    if($staffData) {
        $translator= mysqli_fetch_assoc($staffData);
        if(isset($_GET['id'])) {
        $translatorId = $_GET['id'];
        $staffDocument = displayTranslatedDocumentsForTranslator($translatorId);
        }
    $translatorName = $translator['name'];
    $translatorSurname = $translator['surname'];
    $translatorLanguage = $translator['language'];
    $userPhoneNumber = $translator['phone'];
    $userEmail = $translator['email'];
    $userProfilePicture = $translator['profile_picture_path'];
    $personalNumber = $translator['personalnr'];
    $birthdate = $translator['birthdate'];
    // Generate HTML markup for each translator container
    echo '<div class="individual-container">';
    if(!$userProfilePicture) {
        echo '<img src="images/translator_blank.jpg" alt="Profile Picture">';   
    } else {
    echo '<img src="' . $userProfilePicture . '" alt="Profile Picture">';
    }
    echo "<div class='info_text'>";
    echo "<p class='staff_name'>" . $translatorName . " " . $translatorSurname . "</p>";
    echo "<p class='staff_cred'>Language: " . $translatorLanguage . '</p>';
    echo "<p class='staff_cred'>Phone: " . $userPhoneNumber . '</p>';
    echo "<p class='staff_cred'>Email: " . $userEmail . '</p>';
    echo "<p class='staff_cred'>Birthdate: " . $birthdate . '</p>';
    echo "<p class='staff_cred'>Personal Number: " . $personalNumber . '</p>';
    if(mysqli_num_rows($staffDocument) == 0 ) {
        echo "<p class='projects'>Someone is lazy: Translator has not completed any projects yet.". '</p>';
    } else {
    echo "<p class='projects'>Finished projects info: " . " " . '</p>';
    echo '<table>';
        echo '<tr><th>Document ID</th><th>Submission Date</th><th>Client Name</th><th>Language</th><th>Status</th></tr>';
        while ($row = mysqli_fetch_assoc($staffDocument)) {
            echo '<tr>';
            echo '<td>' . $row['documentid'] . '</td>';
            echo '<td>' . $row['submission_date'] . '</td>';
            echo '<td>' . $row['name'] . " " . $row['surname']. '</td>';
            echo '<td>' . $row['language'] . '</td>';
            if($row['status'] == 2) {
            echo '<td>Finished</td>';
            } else {
                echo '<td>Re Sent</td>';
            }
            echo '</tr>';
        }
        echo '</table>'; 
    }   
    echo '</div>';
    echo '</div>';    
    }
}
?>
<div class="_container"></div>
    </div>
    <script>
          $(document).ready(function() {

var closePopUp = $('.bx-x');

closePopUp.on('click', function() {

$('.buy-title').fadeOut();
});
});
    </script>