<?php 
include "inc/headerblank.php";
?>
<head>
    <link rel="stylesheet" href="css/admin.css">
    <style>
        .sidebar .first_menu li:nth-child(1) a  {
            border-bottom: 2px solid white;
        }
        body {
            overflow-y: unset;
        }
        .translator_wrapper {
            padding-bottom: 3rem;
        }
    </style>
</head>
<nav class="staff_nav">
<p class="staff_title">Manage staff, insert new employees.</p>
<a href="insert_translator.php">Add new!</a>
</nav>
<div class="translator_wrapper">
<?php

$result = fetchTranslatorsForAdmin();

foreach ($result as $row) {
    $staffId = $row['translatorid'];
    $translatorName = $row['name'];
    $translatorSurname = $row['surname'];
    $translatorLanguage = $row['language'];
    $userPhoneNumber = $row['phone'];
    $userEmail = $row['email'];
    $userProfilePicture = $row['profile_picture_path'];
    $translatorId = $row['translatorid'];


    // Generate HTML markup for each translator container
    echo '<div class="translator-container">';
    if(!$userProfilePicture) {
        echo '<img src="images/translator_blank.jpg" alt="Profile Picture">';   
    } else {
    echo '<img src="' . $userProfilePicture . '" alt="Profile Picture">';
    }    echo "<div class='info_text'>";
    echo "<p class='staff_name'>" . $translatorName . " " . $translatorSurname . "</p>";
    echo "<p class='staff_cred'>Language: " . $translatorLanguage . '</p>';
    echo "<p class='staff_cred'>Phone: " . $userPhoneNumber . '</p>';
    echo "<p class='staff_cred'>Email: " . $userEmail . '</p>';
    echo "<span class='view_manage'>View & manage<a href='view_translator.php?id={$translatorId}'>Manage</a></span>";
    echo '</div>';
    echo '</div>';    
}
?>
<div class="_container"></div>
    </div>