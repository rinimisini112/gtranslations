<?php 
include "inc/headerblank.php";
?>
<head>
    <link rel="stylesheet" href="css/blog.css">
    <style>
  .sidebar .first_menu li:nth-child(3) a  {
            border-bottom: 2px solid white;
        }
        body {
            overflow-y: unset;
        }
    </style>
</head>
<section class="blog-container">
    <nav class="blog_menu">
        <ul>
            <li><a href="blog.php">View All Posts</a></li>
            <?php 
             generateBlogMenu();
            ?>
            <form method="GET" action="search_results.php">
    <input type="text" name="query" placeholder="Search...">
    <button type="submit">Search <i class='bx bx-search-alt' ></i></button>
</form>
        </ul>
    </nav>
    <h1 class="blog_introduction">The Blog, write your thoughts away.</h1>
    <div class="post-container">
        <p class="all_posts_title">All posts</p>
        <?php
        // Include your database connection file if not already included
        // Include the fetchBlogPosts function code here
        if(isset($_GET['id']))
        $postId = $_GET['id'];
        $blogPost = viewBlogPostById($postId);

            echo "<div class='blog-post'>";
            echo "<h3 class='single_post_number'>Viewing post NR.{$blogPost['post_id']}</h2>";
            if ($blogPost['picture_path'] !== null) {
                echo "<img class='blog_banner' src='{$blogPost['picture_path']}' alt='Blog Image'>";
            } else {
                echo "<img class='blog_banner' src='images/blog-img.png' alt='Default Blog Image'>";
            }
            echo "<h2 class='single_post-title'>{$blogPost['title']}</h2>";
            $profilePicturePath = $blogPost['profile_picture_path'];
            echo "<div class='author_info'>";
            if(!$profilePicturePath) {
                echo "<img class='author_profile' src='images/default_no_profile.avif' alt='' class='profile-picture'>";
            } else {
            echo "<img class='author_profile' src='$profilePicturePath' alt='Profile Picture' class='profile-pic-mini'>";
            }
            echo "<p class='post-meta'>Posted by {$blogPost['name']} {$blogPost['surname']} on <span class='date'>{$blogPost['created_at']}</span></p>";
            echo "</div>";
            echo "<div class='single_post-content'>{$blogPost['content']}</div>";
            echo "</div>";
        ?>
    </div>
    <?php 
    include "inc/blog_sidebar.php";
    ?>
</section>