<?php 
include "inc/headerblank.php";
?>
<head>
    <link rel="stylesheet" href="css/dashboard.css">
    <style>
        #nav-logo ul li:nth-child(4) {
            border-bottom: 4px solid white;
            border-radius: 8px;
        }
        #nav-logo ul li:nth-child(1) {
            border-bottom: unset;
            border-radius: unset;
        }
    </style>
</head>

<?php
$userid = $_SESSION['userid'];

?>

<section class="doc-progress">
    <h1 class="progress-title">View Current Projects</h1>
    <table>
        <thead>
            <tr>
                <th>User Name</th>
                <th>Request Details</th>
                <th>Deadline</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        <?php   
$translatingRequests = getAllSubmitedDocsForUser($userid);

if (!empty($translatingRequests)) {
    while ($request = mysqli_fetch_assoc($translatingRequests)) {
        $userName = $request['name'] . ' ' . $request['surname']; 
        $documentDetails = $request['document_details'];
        $submissionDate = $request['submission_date'];
        $projectId = $request['documentid'];
        $docUserId = $request['userid'];
        $_SESSION['docId'] = $docUserId;

        if (strlen($documentDetails) > 60) {
            $documentDetails = substr($documentDetails, 0, 50) . " ...";
        }
        ?>
        <tr>
            <td><?php echo $userName; ?></td>
            <td><?php echo $documentDetails; ?></td>
            <td><?php echo $submissionDate; ?></td>
            <td>
                <?php if ($request['status'] == 2): ?>
                    Finished
                <?php elseif ($request['status'] == 1): ?>
                    <a href='view_document.php?id=<?php echo $request['documentid']; ?>'>Declined, Priority!</a>
                <?php else: ?>
                    <a href='view_document.php?id=<?php echo $request['documentid']; ?>'>View Details</a>
                <?php endif; ?>
            </td>
        </tr>
        <?php
    }
} else {
    echo "No translating requests found for the user.";
}
?>
        </tbody>
    </table>
</section>
</section>