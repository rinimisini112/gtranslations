<?php 
include "inc/headerblank.php";
?>
<head>
    <link rel="stylesheet" href="css/workform.css">
</head>

    
<section class="file-form">
    <h1 class="file-title">Enter your info, and start your journey with us.</h1>
<form method="post" enctype="multipart/form-data" id="document">
    
<label for="file">Select your document</label>
    <input type="file" name="file" id="file">
        <label for="language">Select the Language u want it translated to</label>
        <select name="language" id="language">
            <option value="#">Choose</option>
            <?php
    $languages = getAllLanguages();

    while ($language = mysqli_fetch_assoc($languages)) {
        $languageId = $language['languageid'];
        $languageName = $language['language'];

        echo "<option value='$languageId'>$languageName</option>";
    }
    ?>
        </select>
        <label for="details">Enter extra details about your document</label>
        <textarea name="details" id="details"></textarea>
        <label for="deadline">Do you have a deadline? ..Optional</label>
        <input type="datetime-local" name="deadline" id="deadline">
        <p class="note">Note! Once a translator accepts and takes your project, you will be assigned that translator.</p>
        <input type="submit" name="submitFile" value="Submit">
    </form>
    <p class="track-progress-text">Track your documents current progress, assigned translator, etc <a href="workformprogress.php">HERE!</a></p>
    <?php
 if (isset($_POST['submitFile'])) {
    $userId = $_SESSION['userid'];
    $languageId = $_POST['language'];
    $details = $_POST['details'];
    $deadline = ($_POST['deadline'] !== '') ? $_POST['deadline'] : null;
    $submissionDate = date("Y-m-d"); 
    
    $uploadedFilePath = handleFileUpload('file');
    
    // Get translators for the selected language
    $translators = getTranslatorsForLanguage($languageId);

    if (!empty($translators)) {
        // Randomly select one translator from the available translators
        $randomIndex = array_rand($translators);
        $selectedTranslatorId = $translators[$randomIndex];

        // Now, you can call the insertTranslatingRequest function with the selected translator ID
        if ($uploadedFilePath && insertTranslatingRequest($userId, $languageId, $details, $submissionDate, $deadline, $uploadedFilePath, $selectedTranslatorId)) {
            echo "Translating request submitted successfully!";
            // Redirect the user to the progress page after successful submission
            header("Location: users/workformprogress.php");
            exit; // Make sure to add 'exit' after the header to prevent further execution of the current script
        } else {
            echo "Failed to submit translating request. Please try again.";
        }
    } else {
        echo "No translators available for the selected language.";
    }
}

    ?>
</section>