<?php 
include "inc/headerblank.php";
?>
<head>
    <link rel="stylesheet" href="css/dashboard.css">
    <style>
        #nav-logo ul li:nth-child(2) {
    border-bottom: 4px solid white;
    border-radius: 8px;
  }
  #nav-logo ul li:first-child {
    border-bottom:   unset;
    border-radius: unset;
  }
    </style>
</head>

<?php
$userid = $_SESSION['userid'];

?>

<section class="doc-progress">
    <h1 class="progress-title">View Finished Projects</h1>
    <table>
        <thead>
            <tr>
                <th>Client's Name</th>
                <th>Document Details</th>
                <th>Deadline</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <?php
           $translatorId = $_SESSION['translatorid']; // Get the translatorid from the session
               $translatingRequests = getSubmitedDocForTranslator($translatorId);
               // Now $translatingRequests contains the data for the specific translator
           

            foreach ($translatingRequests as $request) {
                $userName = $request['name'] . ' ' . $request['surname']; // Assuming you have a JOIN to get user name from the users table
                $requestDetails = $request['document_details'];
                $submissionDate = $request['submission_date'];
                $projectId = $request['documentid'];
                $docUserId = $request['userid'];
                $status = $request['status'];
                $_SESSION['docId'] = $docUserId;

                if(strlen($requestDetails)>60){
                    $requestDetails=substr($requestDetails,0,50) . " ...";}
            ?>
            <tr>
                <td><?php echo $userName; ?></td>
                <td><?php echo $requestDetails; ?></td>
                <td><?php echo $submissionDate; ?></td>
                <td><?php if($status == 2) {
                    echo "Accepted by user";
                } else if ($status == 1) {
                    echo "Declined";
                } else {
                    echo "Pending..";   
                }
                ?></td>
            </tr>
            <?php
            }
            ?>
        </tbody>
    </table>
    <a href="submit_doc.php" class='go-back'>Go back</a>
</section>
