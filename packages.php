<?php
include "inc/headerblank.php";
?>
<head>
    <link rel="stylesheet" href="css/packages.css">
</head>
<section class="plan-display">
    <p class="display-title">Plan Features</p>
    <div class="toggle-price">
    <span id='highlight-monthly' class="highlighted">Monthly</span>
    <button id="toggle-button" type="button"></button>
    <span id='highlight-yearly'>Yearly</span>
</div>
    <ul class="feature-list">
        <li>Users</li>
        <li>Legal Documents</li>
        <li>Website Translation</li>
        <li>Localization Services</li>
        <li>Interpretation Services</li>
        <li>Blog Access</li>
        <li>File Size</li>
        <li>Tech Support</li>
    </ul>
</section>
<?php 

$userid = $_SESSION['userid'];
if(isset($_POST['yes'])) {
    switchPlan($_POST['plan-id'],$_POST['plan-type'],$_POST['plan-name']);
}
if (isset($_POST['buy-plan'])) {
    if (isset($_POST['plan-type']) && isset($_POST['plan-id'])) {
        $planType = $_POST['plan-type'];
        $planId = $_POST['plan-id'];
        $planName = $_POST['plan-name'];


        if ($planType === "Monthly") {
            echo "<div class='buy-title'>";
            echo "<form method='post' action='" . $_SERVER['PHP_SELF'] . "'>";
            echo "<p>Confirm your Payment <input type='submit' name='confirm' id='switch' value='Confirm'><i class='bx bx-x'></i></p>";
            echo "<input type='hidden' name='plan-type' value='$planType'>";
            echo "<input type='hidden' name='plan-id' value='$planId'>";
            echo "<input type='hidden' name='plan-name' value='$planName'>";
            echo "</form>";
            echo "</div>";
        } else if ($planType === "Yearly") {
            echo "<div class='buy-title'>";
            echo "<form method='post' action='" . $_SERVER['PHP_SELF'] . "'>";
            echo "<p>Confirm your Payment <input type='submit' name='confirmYearly' id='switch' value='Confirm'><i class='bx bx-x'></i></p>";
            echo "<input type='hidden' name='plan-type' value='$planType'>";
            echo "<input type='hidden' name='plan-id' value='$planId'>";
            echo "<input type='hidden' name='plan-name' value='$planName'>";
            echo "</form>";
            echo "</div>";
        }
    }
}

if (isset($_POST['confirmYearly'])) {
    $planType = $_POST['plan-type'];
    $planId = $_POST['plan-id'];
    $planName = $_POST['plan-name'];

    if ($planType === "Yearly") {
        buyPlanYearly($planId, $planType,$planName);
    }
}
if (isset($_POST['confirm'])) {
    $planType = $_POST['plan-type'];
    $planId = $_POST['plan-id'];
    $planName = $_POST['plan-name'];


    if ($planType === "Monthly") {
        buyPlanMonthly($planId, $planType,$planName);
    }
}
?>
<section class="prices-container">
    <div class="price-item">
        <p class="side-title">Basic</p>
        <p class="price-price" id="basic-price">Starting from : <span>9.99$</span></p>
        <ul class="checklist">
            <li>Yes</li>
            <li>Yes</li>
            <li>Yes</li>
            <li>Yes</li>
            <li>Yes</li>
            <li>Yes</li>
            <li>Yes</li>
            <li>Yes</li>
        </ul>
        <form action="" method="post">
        <input type="hidden" name="plan-name" value='Basic'>
            <input type="hidden" name="plan-type" value="Monthly" class="type">
            <input type="hidden" name="plan-id" value="1">
            <input type="submit" name="buy-plan" id="buy-basic" value="Buy Now">
        </form>
    </div>
    <div class="price-item">
        <p class="side-title">Pro</p>
        <p class="price-price" id="pro-price">Starting from : <span>19.99$</span></p>
        <ul class="checklist">
            <li>Yes</li>
            <li>Yes</li>
            <li>Yes</li>
            <li>Yes</li>
            <li>Yes</li>
            <li>Yes</li>
            <li>Yes</li>
            <li>Yes</li>
        </ul>
        <form action="" method="post">
        <input type="hidden" name="plan-name" value='Proffesional'>
        <input type="hidden" name="plan-type" value="Monthly" class="type">
            <input type="hidden" name="plan-id" value="2">
            <input type="submit" name="buy-plan" id="buy-pro" value="Buy Now">
        </form>
    </div>
    <div class="price-item">
        <p class="side-title">Master</p>
        <p class="price-price">Starting from : <span>299$</span></p>
        <ul class="checklist">
            <li>Yes</li>
            <li>Yes</li>
            <li>Yes</li>
            <li>Yes</li>
            <li>Yes</li>
            <li>Yes</li>
            <li>Yes</li>
            <li>Yes</li>
        </ul>
        <form action="" method="post">
            <input type="hidden" name="plan-name" value='Master'>
        <input type="hidden" name="plan-type" value="Yearly" class="type">
        <input type="hidden" name="plan-id" value="3">
        <input type="submit" name="buy-plan" id="buy-master" value="Buy Now">
        </form>
    </div>
</section>
<script>
    $(document).ready(function() {

        var closePopUp = $('.bx-x');
  
        closePopUp.on('click', function() {

    $('.buy-title').fadeOut();
  });
});

 const toggleButton = document.getElementById('toggle-button');
toggleButton.addEventListener('click', function() {
    toggleButton.classList.toggle('active');
    const inputElement = document.querySelectorAll('.type');
    const monthly = document.getElementById('highlight-monthly');
    const yearly = document.getElementById('highlight-yearly');
    const basicPrice = document.getElementById('basic-price');
    const proPrice = document.getElementById('pro-price');
    if (toggleButton.classList.contains('active')) {
        inputElement.forEach(function(inputElement) {
            inputElement.value = "Yearly";
        });
        yearly.classList.add('highlighted');
        monthly.classList.remove('highlighted');
        basicPrice.innerHTML = 'Starting from : <span>99.99$</span>';
        proPrice.innerHTML = 'Starting from : <span>199.99$</span>';
    } else {
        inputElement.forEach(function(inputElement) {
            inputElement.value = "Monthly";
        });
        yearly.classList.remove('highlighted');
        monthly.classList.add('highlighted');
        basicPrice.innerHTML = 'Starting from : <span>9.99$</span>';
        proPrice.innerHTML = 'Starting from : <span>19.99$</span>';
    }
});
</script>