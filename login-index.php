<?php
include "functions.php"
?>
<!DOCTYPE html>
  <html lang="en">
    <head>
      <meta charset="UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <title>Document</title>
      <link rel="stylesheet" href="css/loginform.css" />
      <script src="java-jquery/jquery.js"></script>
      <script src="java-jquery/jquery-ui.js"></script>
      <script src="java-jquery/jquery.validate.js"></script>
      <script src="java-jquery/jquery.validate.min.js"></script>
      <link
        href="https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css"
        rel="stylesheet"
      />
      <style>
        @import url("https://fonts.googleapis.com/css2?family=Oswald:wght@600&family=Playfair+Display:wght@400;600;700&family=Roboto:wght@300;500;700&family=Source+Sans+3:wght@300&family=Ysabeau+SC:wght@300;600&display=swap");
      </style>
    </head>
    <body>
      <header id="nav-logo">
        <p class="title">G Translations<span>.</span></p>
        <div class="mini-nav">
        <a class="home" href='index.php'>Home</a>
          <button
            type="button" class="header-button-register" id="login-register">
            <a href="register-index.php">Sign Up</a>
          </button>
          </div>
      </header>
      <header id="header-container2">
        <div id="frame-login">
            <?php
            if(isset($_POST['login'])) {
              login($_POST['email'],$_POST['password']);
              if(isset($_GET['redirect'])) {
                header("Location: packages.php");
              }
            }
            ?>
            <form method="POST" id="login">
              <h2 class="form-title">Login Into Your Account</h2>
                  <div class="form-row">
                <label for="email">Email :</label>
                <input type="text" class="username" name="email" />
              </div>
              <div class="form-row">
                <label for="password">Password:</label>
                <input type="password" name="password" />
                </div>
                <div class="remember-forgot">
                  <span><a href="">Forgot Your Paswword?</a></span>
                </div>
                <input name="login" type="submit" value="Log in" class="login-btn"/>
              <span class="Account"
                >Don't Have an Account?
               <a href="register-index.php">Sign Up</a></span
              >
            </form>
          </div>
          <script>
              $(document).ready(function () {
          $.validator.addMethod("alphabetsOnly", function(value, element) {
      return /^[a-zA-Z\s]+$/.test(value);
    }, "Enter letters only.");

    $("#login").validate({
      rules: {
        email: {
          required: true,
          email:true,
        },
        password: {
          required: true,
        },
      },
      messages: {
        email: {
          required: "Please enter your email",
          email: "Enter a valid email address",
        },
        password: {
          required: "Please enter your password",
        },
      },
      errorPlacement: function(error, element) {
        error.insertAfter(element); // Place the error message after the input element
      }
    });
              });
          </script>
    
            
          </body>
          </html>