
<?php
include "inc/headerblank.php";

if (isset($_SESSION['userid'])) {
    $userid = $_SESSION['userid'];
    $user = userData($userid);
    $name = $user['name'];
    $surname = $user['surname'];
    $birthdate = $user['birthdate'];
    $personalnr = $user['personalnr'];
    $phone = $user['phone'];
    $email = $user['email'];
    $password = $user['password'];

    if (isset($_POST['save'])) {
        editProfile(
            $_SESSION['userid'],
            $_POST['name'],
            $_POST['surname'],
            $_POST['birthdate'],
            $_POST['personalnr'],
            $_POST['phone'],
            $_POST['email'],
            $_POST['password']
        );
    }

    if (isset($_POST['upload'])) {
        if (isset($_FILES['profilepic'])) {
            $uploadDir = 'user_profile_pictures/';
            $uploadFile = $uploadDir . basename($_FILES['profilepic']['name']);

            if (move_uploaded_file($_FILES['profilepic']['tmp_name'], $uploadFile)) {

                $profilePicturePath = $uploadFile;
                updateProfilePicture($userid, $profilePicturePath);
            }
        }
    }
}
?>

<head>
    <link rel="stylesheet" href="css/profile.css">
</head>
<section class="myaccount">
    <p class="sidebar-title">My Account</p>
    <ul class="user-misc">
        <?php generateProfileNavigation(); ?>
    </ul>
</section>
<section class="accountinfo">
    <p class="detail-title">My Profile <span><a href="profile.php">Back</a></span></p>
    <div class="profilepicture">
        <img src="<?php echo $user['profile_picture_path']; ?>" alt="" class="profile-pic">
        <form method="POST" enctype="multipart/form-data">
            <label for="profilepic">Upload your profile picture!</label>
            <input type="file" name="profilepic" id="profilepic">
            <input type="submit" name="upload" id="upload" value="Upload">
        </form>
    </div>
    <div class="user-details">
    <form method="POST">
                <div class="form-row">
                  <label for="name">Name :</label>
                  <input  type="text" name="name" value="<?php if(!empty($name)) echo $name; ?>"/>
                  </div>
                  <div class="form-row">
                  <label for="surname">Surname :</label>
                  <input type="text" name="surname" value="<?php if(!empty($surname)) echo $surname; ?>"/>
               </div>
                <div class="form-row">
                  <label for="birthdate">Date Of Birth :</label>
                  <input type="date" name="birthdate" value="<?php if(!empty($birthdate)) echo $birthdate; ?>"/>
               </div>
                <div class="form-row">
                <label for="personalnr">Personal Number :</label>
                <input type="tel" name="personalnr" id="personalnr" value="<?php if(!empty($personalnr)) echo $personalnr; ?>"/>
                </div>
          <div class="form-row">
                <label for="phone">Phone Number :</label>
                <input type="tel" name="phone" id="phone" value="<?php if(!empty($phone)) echo $phone; ?>"/>
                </div>
                <div class="form-row">
                  <label for="email">Email :</label>
                  <input type="email" name="email" id="" value="<?php if(!empty($email)) echo $email; ?>"/>
                </div>
                <div class="form-row">
                <label for="password">Password :</label>
                <input type="text" name="password" value="<?php if(!empty($password)) echo $password; ?>"/> 
          </div>
          <input type="submit" name="save" value="Save" id="save">
    </form>
    </div>
</section>
</header>