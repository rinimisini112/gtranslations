<?php 
include "inc/header.php";
?>
<head>
  <link rel="stylesheet" href="css/style.css">
  <style>
    .sidebar .first_menu li:nth-child(1) a  {
            border-bottom: 2px solid white;
        }
  </style>
</head>
<section id="first-section">
    <div class="section-text-box">
        <p class="text-box-name">G TRANSLATIONS</p>
        <p class="text-box-title">Language Bridge: Connecting Cultures</p>
        <p class="text-box-info" id="text-box-info">We are here to assist you with all your language translation needs. Our team of expert translators is dedicated to providing accurate and reliable translations for a wide range of languages and industries. Whether you need document translation or interpretation services, we have you covered.
        Here at our translation agency <span>G Translations</span> we work diligently to ensure that your message is accurately conveyed in the target language. With our extensive experience and expertise, you can trust us to deliver translations that meet your specific requirements.
        We offer competitive pricing and quick turnaround times, without compromising on the quality of our work. We value your satisfaction and strive to exceed your expectations with every project.            
        Trust us with your translation needs, and let us bridge the gap between languages for you.</p>
          </div>
        <div class="image-container">
            <img src="images/albania-fotor-bg-remover-20230628155718.png" alt="">
            <img src="images/serbia-fotor-bg-remover-20230628155836-fotor-bg-remover-2023062816813.png" alt="">
            <img src="images/croatia-fotor-bg-remover-20230628155818.png" alt="">
        </div>
        <a href="#wrapper"><i class='bx bxs-chevrons-down'></i></a>
</section>
    <main id="wrapper">
      <div id="paralax-wrapper">
      <p id="wrapper-title">Journey Across Cultures, Expand Your Reach with Translations<span>.</span></p>
    </div>
    <a href="#second-section"><i class='bx bxs-chevrons-down'></i></a>
    </main>
    <section id="second-section">
      <div class="second-text-box">
      <img src="images/second-sect-img.jpg" alt="" class='second-section-img'>
      <p class="second-box-title">
        <span>Translating</span>
        <span>Your</span>
        <span>Imagination</span> 
        <span class="flickering-dot">.</span>
      </p>
      <p class="second-box-subtitle">What do we offer?</p>
      <ul>
        <li>
          <p class="second-box-info">Discover new language solutions at our translation website. Our skilled translators deliver accurate and reliable translations for diverse industries. From documents, interpretation to websites, we ensure your message is conveyed with precision.</p>
        </li>
        <li>
          <p class="second-box-info">Experience professional service, competitive pricing, and timely deliveries. Break language barriers and expand your global reach with us. Explore our services today</p>
        </li>
        <li>
          <p class="second-box-info">Unlock a world of possibilities with our language solutions. Seamlessly communicate your ideas, products, and services to a global audience. Let your words transcend boundaries and leave an indelible mark.</p>
        </li>
      </ul>
      </div>
      <div class="second-box-img-collage">
        <div class="item"><img src="images/grid-img-1.jpg" alt=""></div>
        <div class="item"><img src="images/grid-img-2.webp" alt=""></div>
        <div class="item"></div>
        <div class="item"><img src="images/grid-img-3.jpg" alt=""></div>
        <div class="item"><img src="images/grid-img-4.jpg" alt=""></div>
        <div class="item"><img src="images/grid-img-5.jpg" alt=""></div>
        <div class="item"></div>
        <div class="item"><img src="images/grid-img-6.jpg" alt=""></div>
        <div class="item"><img src="images/grid-img-7.jpg" alt=""></div>
      </div>
      </section>
      <section id="second-wrapper">
      </section>
    <section id="third-section">
      <div class="plan-info">
        <p class="plan-title">Global Reach</p>
        <p class="plan-subtitle">Your passport to global success starts with accurate translations tailored to your needs.
        </p>
        <?php 
        if(isset($_SESSION['user'])) {
          echo "<button type='button' class='view-service-site'><a href='packages.php'>View Plans</a></button>";
      } else {
        echo "<button type='button' class='view-service-site'><a href='services.php'>View Plans</a></button>";
      }
        ?>
        
      </div>
      <div class="plan-section" id="navigate-toplan">
        <div class="plan basic" id="basic">
          <i class='bx bx-x' id="close-basic"></i>
          <h2 class="plan-heading">Basic</h2>
          <h3 class="plan-price"><span class="dollar">$</span>99</h3>
          <ul class="plan-features">
            <li class="plan-item">Basic Translating Services</li>
            <li class="plan-item">Multi-User Collaboration</li>
            <li class="plan-item">Extended File Size Limit : 3GB</li>
            <li class="plan-item">24/7 Support</li>
          </ul>
          <a href="<?php  
          if(isset($_SESSION['user'])) {
            echo "packages.php"; 
          } else {
           echo "login-index.php?redirect=packages.php"; 
          }  ?>" class="btn buy-now">Buy Now</a>
        </div>
  
        <div class="plan pro" id="pro">
          <i class='bx bx-x' id="close-pro"></i>
          <h2 class="plan-heading">Professional</h2>
          <h3 class="plan-price"><span class="dollar">$</span>199</h3>
          <ul class="black-box">
            <li class="plan-item-white">Enhanced Translating Capabilities</li>
            <li class="plan-item-white">Multi-User Collaboration</li>
            <li class="plan-item-white">Extended File Size Limit : 6GB</li>
            <li class="plan-item-white">Priority Support</li>
          </ul>
          <a href="<?php
            if(isset($_SESSION['user'])) {
            echo "packages.php"; 
          } else {
           echo "login-index.php?redirect=packages.php"; 
          }  ?>" class="btn btn-blue">Buy Now</a>
        </div>
  
        <div class="plan master" id="master">
          <i class='bx bx-x' id="close-master"></i>
          <h2 class="plan-heading">Master</h2>
          <h3 class="plan-price"><span class="dollar">$</span>299</h3>
          <ul class="plan-features">
            <li class="plan-item">Cutting-Edge Translating Tools</li>
            <li class="plan-item">Unlimited User Collaboration</li>
            <li class="plan-item">Unrestricted File Size Limit</li>
            <li class="plan-item">VIP Support</li>
          </ul>
          <a href="<?php
          if(isset($_SESSION['user'])) {
            echo "packages.php"; 
          } else {
           echo "login-index.php?redirect=packages.php"; 
          } 
           ?>" class="btn buy-now">Buy Now</a>
        </div>
      </div>
    </section>
    <section id="third-wrapper">  
    </section>
    <?php 
include "inc/footer.php";
?>