<?php 
include "inc/headerblank.php";
?>
<head>
    <link rel="stylesheet" href="css/blog.css">
    <style>
       .sidebar .first_menu li:nth-child(3) a  {
            border-bottom: 2px solid white;
        }
        body {
            overflow-y: unset;
        }
    </style>
</head>
<section class="blog-container">
    <nav class="blog_menu">
        <ul>
            <li><a href="blog.php">View All Posts</a></li>
            <li><a href="insert_blog.php">Create Your own Blog post</a></li>
            <li><a href="view_user_posts.php">View all your Posts</a></li>
            <form method="GET" action="search_results.php">
    <input type="text" name="query" placeholder="Search...">
    <button type="submit">Search <i class='bx bx-search-alt' ></i></button>
</form>
        </ul>
    </nav>
    <h1 class="blog_introduction">The Blog, view and edit your posts.</h1>
    <div class="post-container">
    <p class="all_posts_title">Edit your blog post</p>
        <?php
       if(isset($_GET['id'])) {
        $postId = $_GET['id'];
        $postData =  fetchBlogPostByPostId($postId);
        $postId = $postData['post_id'];
        $postTitle = $postData['title'];
        $postContent = $postData['content'];
        $postImagePath = $postData['picture_path'];
        $picPath = getExistingPicturePath($postId);
    }
    if (isset($_POST['delete'])) {
        deleteBlogPost($postId);
        echo "<p>Post DELETED succesfully</p>";
        echo "<a href='blog.php'>Go Back!</a>";
        }
        echo "<div class='postNotif'>";
        echo "<p>Post DELETED succesfully</p>";
        echo "<a href='blog.php'>Go Back!</a>";
        echo "</div>"
        ?>
       <form action="" method="post" enctype="multipart/form-data">
    <label for="blogPicture">Blog Picture, insert your own or it will have a standard one selected by Us.</label>
    <input disabled type="file" name="blogPicture" id="blogPicture">
    <div id="imagePreview">
    <?php if (!empty($postImagePath)) : ?>
            <img src="<?php echo $postImagePath; ?>" alt="Blog Picture">
        <?php endif; ?>
    </div>
    <label for="title">Blog Title :</label>
    <input disabled type="text" name="title" id="title" value="<?php if(!empty($postTitle)) echo $postTitle; ?>">
    <label for="content">Content :</label>
    <textarea disabled name="content" id="content"><?php if(!empty($postContent)) echo $postContent; ?></textarea>
    <input type="submit" name='delete' value="Delete Post">
</form>
    </div>
    <?php 
    include "inc/blog_sidebar.php";
    ?>
</section>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        const blogPictureInput = document.getElementById('blogPicture');
        const imagePreview = document.getElementById('imagePreview');
        
        blogPictureInput.addEventListener('change', function(event) {
            const file = event.target.files[0];
            if (file) {
                const reader = new FileReader();
                reader.onload = function(e) {
                    const image = document.createElement('img');
                    image.src = e.target.result;
                    imagePreview.innerHTML = '';
                    imagePreview.appendChild(image);
                };
                reader.readAsDataURL(file);
            } else {
                imagePreview.innerHTML = '';
            }
        });
    });
</script>





