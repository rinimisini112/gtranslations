<?php
include "inc/headerblank.php";
if (isset($_GET['id'])) {
    $userid = $_GET['id'];
    $user = userData($userid);
    $name = $user['name'];
    $surname = $user['surname'];
?>
    <style>
        .sidebar .first_menu li:nth-child(4) a  {
            border-bottom: 2px solid white;
        }
    </style>
    <head>
        <link rel="stylesheet" href="css/admin.css">
    </head>
    <section class="accountinfo">
        <div class="detail-title"><p><a href="users_managment.php"><i class='bx bx-chevrons-left'></i></a>Viewing :
            <?php
            echo "$name" . " " . "$surname";
            ?></p> 
            <div class="crud_butons">
                 <a href="editUserProfile.php?id=<?php echo  $userid?>">Edit</a>
                 <form method="post">
                    <input class='deleteUser' type="submit" name="deleteUser" value='Delete'>
                </form>
            </div>
        </div>
        <div class="profilepicture">
            <?php
             $profilePicturePath = $user['profile_picture_path'];
             if(!$profilePicturePath) {
                echo "<img src='images/default_no_profile.avif' alt='' class='profile-picture'>";
             } else {
                echo "<img src='$profilePicturePath' alt='Profile Picture' class='profile-pic-mini'>";
             }
             ?>
        </div>
        <div class="user-details">
        <?php
        $birthdate = $user['birthdate'];
        $personalnr = $user['personalnr'];
        $phone = $user['phone'];
        $email = $user['email'];
        $role = $user['role'];
        $transID = $user['translatorid'];
        $password = $user['password'];
    }
    if (isset($_POST['deleteUser'])) {
        deleteUserForAdmin($userid);
        header("Location:users_management.php");
    }
        ?>
<form method="POST">
    <div class="form-grid">
        <div class="form-column">
            <div class="form-row">
                <label for="Role">Role :</label>
                <input disabled type="role" id="role" name="role" value="<?php loadRoleForAdmin($role); ?>" />
            </div>
            <div class="form-row">
                <label for="name">Name :</label>
                <input disabled type="text" id="name" name="name" value="<?php if (!empty($name)) echo $name; ?>" />
            </div>
            <div class="form-row">
                <label for="surname">Surname :</label>
                <input disabled type="text" id="surname" name="surname" value="<?php if (!empty($surname)) echo $surname; ?>" />
            </div>
            <div class="form-row">
                <label for="birthdate">Date Of Birth :</label>
                <input disabled type="date" id="birthdate" name="birthdate" value="<?php if (!empty($birthdate)) echo $birthdate; ?>" />
            </div>
        </div>
        <div class="form-column">
            <div class="form-row">
                <label for="personalnr">Personal Number :</label>
                <input disabled type="tel" id="personalnr" name="personalnr" id="personalnr" value="<?php if (!empty($personalnr)) echo $personalnr; ?>" />
            </div>
            <div class="form-row">
                <label for="phone">Phone Number :</label>
                <input disabled type="tel" id="phone" name="phone" id="phone" value="<?php if (!empty($phone)) echo $phone; ?>" />
            </div>
            <div class="form-row">
                <label for="email">Email :</label>
                <input disabled type="email" id="email" name="email" id="" value="<?php if (!empty($email)) echo $email; ?>" />
            </div>
            <div class="form-row">
                <label for="password">Password :</label>
                <input disabled type="password" id="password" name="password" id="" value="<?php if (!empty($password)) echo $password; ?>" />
            </div>
        </div>
    </div>
</form>
        </div>
    </section>
    </header>