<?php
include "inc/header.php";
?>
<head>
    <link rel="stylesheet" href="css/aboutus.css">
</head>
<body>
    
<script src="java-jquery/minified/gsap.min.js"></script>
<script src="java-jquery/minified/ScrollTrigger.min.js"></script>
<section class="about-us_first-part" id="aboutUsWrapper">
    <p class="welcome-title fade-in-right">Welcome to G Translations</p>
    <p class="intro-title fade-in-right">Introduction :</p>
    <p class="office-info fade-in-right">Welcome to G Translations - Your Trusted Translation Office!</p>
    <ul>
        <li class="fade-in-right">We are your dedicated translation office. Our goal is to help you communicate across languages, connecting you with people all around of the globe.
Our team is group of language enthusiasts and skilled linguists. We take pride in delivering accurate and relevant translations that resonate with your audience.</li>
        <li class="fade-in-right">Whether you're an individual seeking to translate personal documents or a business looking to expand internationally, we are here to support you. With our attention to detail and commitment to quality, you can trust that your message will be delivered with utmost precision.</li>
        <li class="fade-in-right">Join us on this exciting journey of exploration and collaboration. Let G Translations be your go-to translation partner, and together, we'll conquer language barriers and unlock new opportunities.</li>
        <li class="fade-in-right">Welcome to G Translations</li>
    </ul>
</section>
<section class="about-us_second-part">
<p class="origin-title fade-in-left">Tale of Us</p>
    <ul>
        <li class="fade-in-left">Once upon a time, in a bustling city, there lived three language enthusiasts: Alex, Maya, and Leo. Each of them had a deep love for languages and a shared dream of bridging communication gaps across the world.
        One fateful day, as they sipped coffee at their favorite café, an idea sparked like lightning. Why not combine their linguistic talents and create something extraordinary? And so, G Translations was born!
        </li>
        <li class="fade-in-left">Fuelled by their passion, they set out on a thrilling adventure to make the world a smaller, more connected place. They scoured the globe, immersing themselves in diverse cultures, learning new languages, and honing their translation skills.</li>
        <li class="fade-in-left">As word of their exceptional abilities spread, G Translations soon found itself in high demand. Their accurate and culturally sensitive translations resonated with people from all walks of life, making them the go-to language experts in the city.
        With each new project, the trio's bond grew stronger, and their vision clearer. G Translations wasn't just a translation office; it was a force of unity, turning language barriers into bridges of understanding.
        </li>
        <li class="fade-in-left">Years passed, and G Translations became synonymous with reliability and top-notch quality. From humble beginnings, the little language venture had transformed into a thriving hub of linguistic brilliance.
        The origin of G Translations is a testament to the power of passion, unity, and the belief that words have the ability to transcend borders. And so, the story of G Translations lives on.</li>
        </li>
    </ul>
</section>

<div class="wrap">
<section class="reveal">
<h1 class="g-title">Timestamp of <span>G Translations</span></h1>
<div class="reveal-wrap">
    <div class="reveal-img">
        <div class="reveal-img-item">
            <span class="reveal-img-num">001</span>
                <div class="reveal-img-inner">
                    <div class="reveal-img-bl">
                        <img src="images/timestamp1.webp" alt="" class="reveal-img-img">
                    </div>
              </div>
        </div>
        <div class="reveal-img-item">
            <span class="reveal-img-num">002</span>
                <div class="reveal-img-inner">
                    <div class="reveal-img-bl">
                        <img src="images/timestamp2.jpeg" alt="" class="reveal-img-img">
                    </div>
              </div>
        </div>
        <div class="reveal-img-item">
            <span class="reveal-img-num">003</span>
                <div class="reveal-img-inner">
                    <div class="reveal-img-bl">
                        <img src="images/timestamp3.jpeg" alt="" class="reveal-img-img">
                    </div>
              </div>
        </div>
        <div class="reveal-img-item">
            <span class="reveal-img-num">004</span>
                <div class="reveal-img-inner">
                    <div class="reveal-img-bl">
                        <img src="images/timestamp4.webp" alt="" class="reveal-img-img">
                    </div>
              </div>
        </div>
    </div>
</div>
</section>
</div>
<script>

 gsap.registerPlugin(ScrollTrigger);

    function fadeInRight(elements) {
      // ScrollTrigger for scrolling from the top down
      gsap.from(elements, {
        x: 300,
        opacity: 0,
        duration: 1.5,
        ease: "power2.out",
        stagger: 0.3, // Add a slight delay between each element's animation
        scrollTrigger: {
          trigger: "#aboutUsWrapper", // Use the container element as the trigger
          start: "top 80%", // Start the animation when the top of the container is 80% in view
          end: "bottom bottom", // End the animation when the bottom of the container reaches the bottom of the viewport
          toggleActions: "restart none play play" // Restart the animation when the start point is reached, play when scrolling down
        }
      });
    }
    function fadeInLeft(elements) {
    gsap.from(elements, {
        x: -300,
        opacity: 0,
        duration: 1.5,
        ease: "power2.out",
        stagger: 0.3, // Add a slight delay between each element's animation
        scrollTrigger: {
            trigger: ".about-us_second-part", // Use the first element in the group as the trigger
            start: "top 80%", // Start the animation when the top of the element is 80% in view
            end: "bottom bottom", // End the animation when the bottom of the element reaches the bottom of the viewport
            toggleActions: "restart none play play" // Restart the animation when the start point is reached, play when scrolling down
        }
    });
    "use strict";

    const gsapItem = gsap.utils.toArray('.reveal-img-item');

    gsapItem.forEach((gsIt) => {

        const imgNum = gsIt.querySelector('.reveal-img-num');
        const imgInner = gsIt.querySelector('.reveal-img-inner');
        const imgBl = gsIt.querySelector('.reveal-img-bl');
        const imgImg = gsIt.querySelector('.reveal-img-img');
        
        let tl = gsap.timeline({
            scrollTrigger: {
                trigger: gsIt,
                start: "top 70%",
                toggleActions: "restart none restart none ",
            }
        });
        tl.from(imgNum, 1, { opacity: 0, translateY: "80px", ease: "expo.out" })
        tl.to(imgInner, .5, {clipPath: "polygon(0% 0%,100% 0%,100% 100%,0% 100%)", ease: "expo.out"}, "-=1")
        tl.to(imgBl, 1.5, {clipPath: "polygon(0% 0%,100% 0%,100% 100%,0% 100%)", ease: "expo.out"}, "-=0.5")
        tl.from(imgImg, 4, {scale: 1.5,filter:"blur(15px)", ease: "expo.out"}, "-=1.7")

    });
}

    const fadeInRightElements = document.querySelectorAll(".fade-in-right");
    fadeInRight(fadeInRightElements);
    const fadeInLeftElements = document.querySelectorAll(".fade-in-left");
fadeInLeft(fadeInLeftElements);
</script>
<main class="about-us-wrapper">
    <?php 
    if(isset($_SESSION['user'])) {
        echo "<a href='packages.php'><button type='button' class='wrapper-button'>Start your journey</button></a>";
    } else 
        {
            echo "<a href='login-index.php?redirect=packages.php'><button type='button' class='wrapper-button'>Start your journey</button></a>";
        }
    ?>
</main>
</body>
<?php
include "inc/footer.php";
?>