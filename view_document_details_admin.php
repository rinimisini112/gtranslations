<?php 
include "inc/headerblank.php";
$totalFinishedDocs = getTotalFinishedDocuments();
$totalFinishedDocsFormatted = str_pad($totalFinishedDocs, 2, '0', STR_PAD_LEFT); // Format the total as double digits
?>
<head>
    <link rel="stylesheet" href="css/admin_dashboard.css">
    <style>
        body {
            overflow-y: unset;
        }
        .sidebar .first_menu li:nth-child(2) a  {
            border-bottom: 2px solid white;
        }
        #header-container {
            display: flex;
      }
      
    </style>
</head>
<h1 class="intro_title">View all documents progress, status, and details.</h1>
<div class="info_subtitle">
    <h2>Documents that have the same 'Doc ID' are connected, meaning they have the same Client and assigned Translator in both of them.</h2>
    <span class="counter_text">Total Document counter :</span>
    <span class="double_digit_counter"><?php echo $totalFinishedDocsFormatted; ?></span>
</div>
<section class="doc-container">
    <?php if(isset($_GET['rid'])) {
                $requestId = $_GET['rid'];
        echo "<h1 class='progress-title'><a href='admin_dashboard.php'><i class='bx bx-chevrons-left'></i></a>Document Requests - Viewing Request Nr.". $requestId . "</h1>";
        $requestData = singleRequestDataAdmin($requestId);
        if (!empty($requestData)) {
            $name = $requestData['name'];
            $surname = $requestData['surname'];
            $clientName = $requestData['clientName'];
            $clientSurname = $requestData['clientSurname'];
            $request_details = $requestData['request_details'];
            $deadline = $requestData['deadline'];
            $status = $requestData['status'];
            $language = $requestData['language'];
            $profilePicture =$requestData['profile_picture_path'];
            $filePath2= $requestData['document_file'];
            $translatorId = $requestData['translatorid'];
            $translatorPic = getTranslatorPicture($translatorId);
            $translatorPicture = $translatorPic['profile_picture_path'];
        } else {
            echo "No data found for requestId: $requestId";
        }
    } else if (isset($_GET['did'])) {
        $documentId = $_GET['did'];
        echo "<h1 class='progress-title'><a href='admin_dashboard.php'><i class='bx bx-chevrons-left'></i></a>Finished Documents - Viewing Document Nr.". $documentId . "</h1>";
        $documentData = singleDocumentDataAdmin($documentId);
        if (!empty($documentData)) {
        $profilePicture =$documentData['profile_picture_path'];
        $name = $documentData['name'];
            $surname = $documentData['surname'];
            $clientName = $documentData['clientName'];
            $clientSurname = $documentData['clientSurname'];
            $documentDetails = $documentData['document_details'];
            $submissionDate = $documentData['submission_date'];
            $docStatus = $documentData['status'];
            $docLanguage = $documentData['language'];
            $profilePicture =$documentData['profile_picture_path'];
            $file = $documentData['finished_doc'];
            $translatorId = $documentData['translatorid'];
            $translatorPic = getTranslatorPicture($translatorId);
            $translatorPicture = $translatorPic['profile_picture_path'];
        } else {
        echo "No data found for requestId: $requestId";
    }
    }
    echo '<div class="client_translator_card">';
            echo '<div class="card">';
            echo "<img src='{$profilePicture}' class='card_profile' alt=''>";
            echo '<p class="doc_info">Client\'s Name: ' . $clientName . ' ' . $clientSurname . '</p>';
            echo '</div>';
            echo '<div class="card">';
            echo "<img src='{$translatorPicture}' class='card_profile'alt=''>";
            echo '<p class="doc_info">Assigned Translator: ' . $name . ' ' . $surname . '</p>';
            echo '</div>';
            echo '</div>';
            if(isset($_GET['rid'])) {
                if ($filePath2) {
                    $filename = basename($filePath2); 
                    $filePath = "submits/" . $filename;
                    echo '<p class="doc_info">View File : '."<a href='javascript:void(0);' onclick='downloadDocument(\"$filePath\")'>Download Document</a></p>";        
                } else {
                    echo "Document not available.";
                }            echo '<p class="doc_info">Request Details : ' . $request_details. '</p>';
            echo '<p class="doc_info">Deadline : ' . $deadline. ' </p>';
            echo '<p class="doc_info">Language : ' . $language. ' </p>';
            if($status == 2) {
                echo "<p class='doc_info'>Status : <span class='green'>Finished.</span></p>"; 
            } else if ($status == 1) {
                echo "<p class='doc_info'>Status : <span class='red'>Re Sent!</span></p>"; 
            } else {
                echo "<p class='doc_info'>Status : <span class='gray'>Pending...</span></p>"; 
            }
                    }
             else if (isset($_GET['did'])){
                if ($file) {
                    $filename = basename($file); 
                    $filePath = "submits/" . $filename;
                    echo '<p class="doc_info">View File : '."<a href='javascript:void(0);' onclick='downloadDocument(\"$filePath\")'>Download Document</a></p>";        
                } else {
                    echo "Document not available.";
                }

            echo '<p class="doc_info">Document Details : ' . $documentDetails. ' </p>';
            echo '<p class="doc_info">Submission Date : ' . $submissionDate. ' </p>';
            echo '<p class="doc_info">Language: ' . $docLanguage. ' </p>';
            if($docStatus == 2) {
                echo '<p class="doc_info green">Status : Finished</p>'; 
            } else if ($docStatus == 1) {
                echo '<p class="doc_info red">Status : Re Sent</p>';                 
            } else {
                echo '<p class="doc_info gray">Status : Pending</p>';                 
            }
            }?>
</section>