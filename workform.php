<?php 
include "inc/headerblank.php";
?>
<head>
    <link rel="stylesheet" href="css/workform.css">
    <style>
        body {
            overflow-y: unset;
        }
 
    </style>
</head>

    
<section class="file-form">
    <h1 class="file-title">Enter your info, and start your journey with us.</h1>
<form method="post" enctype="multipart/form-data" id="document">
    
<label for="file">Select your document</label>
    <input type="file" name="file" id="file" required title='Upload your Document'>
    <div id="errorContainer"></div>
        <label for="language">Select the Language u want it translated to</label>
        <select name="language" id="language" required title="Please select a language">
            <option value="0">Choose</option>
            <?php
    $languages = getAllLanguages();

    while ($language = mysqli_fetch_assoc($languages)) {
        $languageId = $language['languageid'];
        $languageName = $language['language'];

        echo "<option value='$languageId'>$languageName</option>";
    }
    ?>
        </select>
        <label for="details">Enter extra details about your document</label>
        <textarea name="details" id="details"></textarea>
        <label for="deadline">Enter your deadline</label>
        <input type="datetime-local" name="deadline" id="deadline">
        <p class="note">Note! Once a translator accepts and takes your project, you will be assigned that translator.</p>
        <input type="submit" name="submitFile" value="Submit" class="submitDoc">
    </form>
    <p class="track-progress-text">Track your documents current progress, assigned translator, etc <a href="workformprogress.php">HERE!</a></p>
    <?php

 if (isset($_POST['submitFile'])) {
    $userId = $_SESSION['userid'];
    $languageId = $_POST['language'];
    $details = $_POST['details'];
    $deadline = ($_POST['deadline'] !== '') ? $_POST['deadline'] : null;
    $submissionDate = date("Y-m-d"); 
    
    $uploadedFilePath = handleFileUpload('file');
    
    $translators = getTranslatorsForLanguage($languageId);

    if (!empty($translators)) {
        $randomIndex = array_rand($translators);
        $selectedTranslatorId = $translators[$randomIndex];

        if ($uploadedFilePath && insertTranslatingRequest($userId, $languageId, $details, $submissionDate, $deadline, $uploadedFilePath, $selectedTranslatorId)) {
            echo "<div class='message' id='closePopUp'><p>Translating request submitted successfully!</p>";
            echo "<button class='closeMesagge'>Close</button>";
            echo "</div>"; 
        } else {
            echo "Failed to submit translating request. Error: " . mysqli_error($dbconn);        }
    } else {
        echo "No translators available for the selected language.";
    }
}           

    ?>
</section>
<script>
$(document).ready(function() {
  function closePopUp() {
    $('#closePopUp').fadeOut();
  }

  $('.closeMesagge').click(function() {
    closePopUp();
  });
});
$(document).ready(function () {
          $.validator.addMethod("alphabetsOnly", function(value, element) {
      return /^[a-zA-Z\s]+$/.test(value);
    }, "Enter letters only.");


          // Add validation rules and messages to your registration form
          $.validator.addMethod("valueNotEquals", function(value, element, arg){
  return arg !== value;
 }, "Value must not equal arg.");
        $('#document').validate({
            rules: {
                language: { 
                valueNotEquals: "0",
                },
                details: {
                    required: true,
                },
                deadline: {
                    required: true,
                }
            },
            messages: {
                language: {
                    valueNotEquals: "Please select a language",
                },
                details: {
                    required: "Please enter any details u have",
                },
                deadline: {
                    required: "Please enter your deadline"
                },
              
        }});
    })

</script>