<?php 
include "inc/headerblank.php";
?>
<head>
    <link rel="stylesheet" href="css/blog.css">
    <style>
        .sidebar .first_menu li:nth-child(3) a  {
            border-bottom: 2px solid white;
        }
        body {
            overflow-y: unset;
        }
        .message {
            height: 3rem;
            width: 100%;
            font-weight: 700;
            font-size: 1.5rem;
            font-family: Arial, Helvetica, sans-serif;
            display: flex;
            align-items: center;
            background-color: white;
            color: black;
            padding-left: 1rem;
        }
        .message a {
            padding-left: 1rem;
            color: black;
            font-weight: 700;
            font-size: 1.5rem;
            font-family: Arial, Helvetica, sans-serif;
        }
    </style>
    </style>
</head>
<section class="blog-container">
    <nav class="blog_menu">
        <ul>
            <li><a href="blog.php">View All Posts</a></li>
            <li><a href="insert_blog.php">Create Your own Blog post</a></li>
            <li><a href="view_user_posts.php">View all your Posts</a></li>
            <form method="GET" action="search_results.php">
    <input type="text" name="query" placeholder="Search...">
    <button type="submit">Search <i class='bx bx-search-alt' ></i></button>
</form>
        </ul>
    </nav>
    <h1 class="blog_introduction">The Blog, write your own post.</h1>
    <div class="post-container">
    <p class="all_posts_title">Write your blog</p>
        <?php
       if(isset($_POST['submit'])) {
        $pictureName = $_FILES['blogPicture']['name'];
        $pictureTemp = $_FILES['blogPicture']['tmp_name'];
        
        $uploadDir = 'blog_pictures/'; // Directory to store uploaded pictures
        $picturePath = $uploadDir . $pictureName;
        
        move_uploaded_file($pictureTemp, $picturePath);
        
        insertBlogPost($_POST['title'], $_POST['content'], $_SESSION['userid'], $picturePath);
        echo "<p class='message'>Blog Post created succesfully<a href='view_user_posts.php'>View..</a></p>";
    }

        ?>
       <form action="" id='insert_blog' method="post" enctype="multipart/form-data">
    <label for="blogPicture">Blog Picture, insert your own or it will have a standard one selected by Us.</label>
    <input type="file" name="blogPicture" id="blogPicture">
    <div id="imagePreview"></div>
    <label for="title">Blog Title :</label>
    <input type="text" name="title" id="title">
    <label for="content">Content :</label>
    <textarea name="content" id="content"></textarea>
    <input type="submit" name='submit' value="Add Post" class="addPost">
</form>
    </div>
    <?php 
    include "inc/blog_sidebar.php";
    ?>
</section>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        const blogPictureInput = document.getElementById('blogPicture');
        const imagePreview = document.getElementById('imagePreview');
        
        blogPictureInput.addEventListener('change', function(event) {
            const file = event.target.files[0];
            if (file) {
                const reader = new FileReader();
                reader.onload = function(e) {
                    const image = document.createElement('img');
                    image.src = e.target.result;
                    imagePreview.innerHTML = '';
                    imagePreview.appendChild(image);
                };
                reader.readAsDataURL(file);
            } else {
                imagePreview.innerHTML = '';
            }
        });
    });


        $(document).ready(function () {
          $.validator.addMethod("alphabetsOnly", function(value, element) {
      return /^[a-zA-Z\s]+$/.test(value);
    }, "Enter letters only.");


          // Add validation rules and messages to your registration form
          $("#insert_blog").validate({
            rules: {
              title: {
                required: true,
                minlength:8,
              },
              content: {
                required: true,
                minlength:50,
              },
            },
            messages: {
              title: {
                required: "This box should not be blank",
                minlength: "Titile should be longer than 8 characters",
            },
              content: {
                required: "This area should not be left blank",
                minlength: "Minimum 50 characters for content",
              },
            },
            errorPlacement: function (error, element) {
                // Place the error message below the input element
                error.insertAfter(element);
            }
        });
          });
    </script>





