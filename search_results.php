<?php 
include "inc/headerblank.php";
?>
<head>
    <link rel="stylesheet" href="css/blog.css">
    <style>
        .sidebar .first_menu li:nth-child(3) a  {
            border-bottom: 2px solid white;
        }
        body {
            overflow-y: unset;
        }
    </style>
</head>
<section class="blog-container">
    <?php 
       if (isset($_GET['query']) && !empty($_GET['query'])) {
        $searchQuery = $_GET['query'];
        $searchResults = searchBlogPosts($searchQuery);

    ?>
<nav class="blog_menu">
        <ul>
            <li><a href="blog.php">View All Posts</a></li>
            <?php 
             generateBlogMenu();
            ?>
             <form method="GET" action="search_results.php">
    <input type="text" name="query" placeholder="Search...">
    <button type="submit">Search <i class='bx bx-search-alt' ></i></button>
</form>
        </ul>
    </nav>
    <h1 class="blog_introduction">The Blog, search results.</h1>
    <div class="post-container">
        <p class="all_posts_title">Search Results for '<?php echo $searchQuery;?>'</p>
        <?php
     
            if (!empty($searchResults)) {
                foreach ($searchResults as $post) {
                    // Display search result entries similar to how you display regular blog posts
                    echo "<div class='blog-post'>";
            if ($post['picture_path'] !== null) {
                echo "<img class='blog_banner' src='{$post['picture_path']}' alt='Blog Image'>";
            } else {
                echo "<img class='blog_banner' src='images/blog-img.png' alt='Default Blog Image'>";
            }
            echo "<h2 class='post-title'>{$post['title']}</h2>";
            $profilePicturePath = $post['profile_picture_path'];
            echo "<div class='author_info'>";
            if(!$profilePicturePath) {
                echo "<img class='author_profile' src='images/default_no_profile.avif' alt='' class='profile-picture'>";
            } else {
            echo "<img class='author_profile' src='$profilePicturePath' alt='Profile Picture' class='profile-pic-mini'>";
            }
            echo "<p class='post-meta'>Posted by {$post['name']} {$post['surname']} on <span class='date'>{$post['created_at']}</span></p>";
            echo "</div>";
            $limitedContent = substr($post['content'], 0, 150);
            if (strlen($post['content']) > 150) {
                $limitedContent .= "...<a href='blog_post.php?id={$post['post_id']}'>See More!</a>"; // Add ellipsis if content is longer than 50 characters
            } else {
                $limitedContent .= "...<a href='blog_post.php?id={$post['post_id']}'>See More!</a>";
            }
            
            echo "<div class='post-content'>$limitedContent</div>";
            
            echo "</div>";
                }
            } else {
                echo "No results found.";
            }
        } else {
            echo "Enter a search query.";
        }
        ?>
    </div>
    <div class="socials_news">
    <p class="all_posts_title">Stay in touch with us</p>
       <div class="newsletter">
       <p class="newsletter-text">Subscribe to our newsletter to Stay notified with news</p>
         <?php
          /*if (isset($_POST['submitNewsletter'])) {
          newsletter($_POST['email']);
          }
          sendNewsletter();*/
         ?>
          <form action="" method="POST">
          <input type="email" name="email" id="email" placeholder="Your Email">
          <input type="submit" name="submitNewsletter" value="Subscribe">
        </form>
        <p class="newsletter-text"></p>
       </div>
       <div class="social_media">
        <p class="social_media_title">Our Socials</p>
        <a href="#"><i class='bx bxl-twitter'></i></a>
        <a href="#"><i class='bx bxl-facebook-circle'></i></a>
        <a href="#"><i class='bx bxl-instagram'></i></a>
        <a href="#"><i class='bx bx-globe'></i></a>
        <a href="#"><i class='bx bxl-github'></i></a>
       </div>
       <div class="tags_table">
    <p class="social_media_title">Popular Tags</p>
    <table>
        <tr>
            <td><a href="#">Translation Services</a></td>
            <td><a href="#">Document Translation</a></td>
        </tr>
        <tr>
            <td><a href="#">Website Localization</a></td>
            <td><a href="#">Certified Translations</a></td>
        </tr>
        <tr>
            <td><a href="#">Language Pairs</a></td>
            <td><a href="#">Professional Translators</a></td>
        </tr>
        <tr>
            <td><a href="#">Business Translation</a></td>
            <td><a href="#">Technical Translation</a></td>
        </tr>
        <tr>
            <td><a href="#">Legal Translation</a></td>
            <td><a href="#">Medical Translation</a></td>
        </tr>
        <tr>
            <td><a href="#">Financial Translation</a></td>
            <td><a href="#">Marketing Translation</a></td>
        </tr>
        <tr>
            <td><a href="#">Website Translation</a></td>
            <td><a href="#">Software Localization</a></td>
        </tr>
        <tr>
            <td><a href="#">Video Subtitling</a></td>
            <td><a href="#">Audio Transcription</a></td>
        </tr>
    </table>
</div>
        </div></section>