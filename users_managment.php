<?php 
include "inc/headerblank.php";
?>
<head>
    <link rel="stylesheet" href="css/admin.css">
    <style>
        .sidebar .first_menu li:nth-child(4) a  {
            border-bottom: 2px solid white;
        }
        #header-container {
            margin-top: 6rem;
        }
        body {
            overflow-y: unset;
        }
    </style>
</head>
<main class="user_container">
    <div class="view_search_users">
    <h1 class="management_title">View, manage, and delete users.</h1>
    <form method="GET" action="users_results.php" class="search_users">
    <span>Search Users based on name or other data</span>
    <input type="text" name="query" placeholder="Search...">
    <button type="submit">Search <i class='bx bx-search-alt'></i></button>
</form>    
</div>
    <table id="members">
            <tr>
                <th>Name</th>
                <th>Date of Birth</th>
                <th>Personal Number</th>
                <th>Phone Number</th>
                <th>Email</th>
                <th>Role</th>
                <th>Manage</th>
            </tr>
            <?php
            $result = selectUserForAdmin();
            $i = 0;
            while ($user = mysqli_fetch_assoc($result)) {
                $userid = $user['userid'];
                if ($i % 2 == 0) {
                    echo "<tr>";
                } else {
                    echo "<tr class='alt'>";
                }
                echo "<td>" . $user['name'] . ' ' . $user['surname'] . "</td>";
                echo "<td>" . $user['birthdate'] . "</td>";
                echo "<td>" . $user['personalnr'] . "</td>";
                echo "<td>" . $user['phone'] . "</td>";
                echo "<td>" . $user['email'] . "</td>";
                if($user['role'] == 2) {
                    echo "<td>Admin</td>";
                } else if($user['role'] == 1) {
                    echo "<td>Translator</td>";
                } else {
                    echo "<td>User</td>";
                }
                if ($user['role'] == 2) {
                    echo "<td class='admin_sign'><i class='bx bxs-user-rectangle'></i></td>";
                } else {
                echo "<td><a href='edit_users.php?id=$userid'><i class='bx bx-dots-horizontal-rounded'></i><i class='bx bx-edit'></i></a></td>";
                }
                echo "</tr>";
            }
            ?>

        </table>
</main>