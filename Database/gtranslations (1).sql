-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 20, 2023 at 10:00 PM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gtranslations`
--

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `clientid` int(11) NOT NULL,
  `subscription_type` varchar(20) NOT NULL,
  `application_date` date DEFAULT NULL,
  `expiration_date` date DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `subid` int(11) DEFAULT NULL,
  `plan_name` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`clientid`, `subscription_type`, `application_date`, `expiration_date`, `userid`, `subid`, `plan_name`) VALUES
(53, 'Monthly', '2023-09-03', '2023-10-03', 17, 1, 'Basic'),
(65, 'Monthly', '2023-09-08', '2023-10-08', 8, 1, 'Basic');

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `languageid` int(11) NOT NULL,
  `language` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`languageid`, `language`) VALUES
(1, 'English'),
(2, 'Serbian'),
(3, 'Croatian'),
(4, 'Albanian');

-- --------------------------------------------------------

--
-- Table structure for table `newsletter`
--

CREATE TABLE `newsletter` (
  `newsid` int(11) NOT NULL,
  `email` varchar(30) NOT NULL,
  `subscription_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `newsletter`
--

INSERT INTO `newsletter` (`newsid`, `email`, `subscription_date`) VALUES
(1, 'test1@gmail.com', '2023-07-23 19:36:28'),
(3, 'misinirini@gmail.com', '2023-07-23 19:50:57'),
(4, 'genthoxha@gmail.com', '2023-07-23 20:17:37'),
(5, 'endritg@gmail.com', '2023-07-23 20:19:51'),
(6, 'test2@gmail.com', '2023-07-24 16:04:05');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `post_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `userid` int(11) NOT NULL,
  `picture_path` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`post_id`, `title`, `content`, `created_at`, `userid`, `picture_path`) VALUES
(1, 'A Journey Through Europe', 'Embarking on a once-in-a-lifetime adventure through the charming cities of Europe. From the romantic streets of Paris to the historic ruins of Rome, join me as I share my experiences, tips, and stunning photographs from this unforgettable trip.', '2023-08-06 20:43:42', 1, 'blog_pictures/rafaelo resort.jpg'),
(2, 'Exploring the Enchanting Landscapes of New Zealand', 'Embarking on a soul-stirring journey across the breathtaking landscapes of New Zealand. From the snow-capped peaks of the Southern Alps to the pristine beaches of the Abel Tasman National Park, join me as I recount my awe-inspiring adventures, share valuable travel tips, and reflect on the profound connection between nature and the human spirit.', '2023-08-06 20:44:36', 1, NULL),
(3, 'Indulge Your Senses with Decadent Chocolate Truffle Cake', 'Satisfy your sweet cravings with a sinfully rich and irresistibly decadent chocolate truffle cake. In this comprehensive recipe, I guide you through the art of creating a luscious cake that melts in your mouth, topped with velvety ganache and adorned with edible gold flakes. Whether for a special occasion or a moment of self-indulgence, this dessert is sure to leave a lasting impression.', '2023-08-06 20:47:17', 2, NULL),
(4, 'Unveiling the Innovations Shaping the Future of Space Exploration', 'Journey into the cosmos as we explore the cutting-edge technologies revolutionizing space exploration. From reusable rocketry to ambitious plans for lunar colonization, this in-depth article delves into the remarkable advancements propelling humanity towards the stars. Join me in contemplating the boundless possibilities and the potential to redefine our understanding of the universe.', '2023-08-06 20:47:56', 3, NULL),
(5, 'Elevate Your Wardrobe with Timeless Vintage Fashion', 'Step into a world of vintage elegance and timeless style with a curated collection of fashion inspiration from bygone eras. From the glamorous allure of 1920s flapper dresses to the iconic silhouettes of the 1950s, I take you on a visual journey through fashion history, providing tips on how to incorporate vintage pieces into your modern wardrobe with flair.', '2023-08-06 20:48:29', 4, NULL),
(10, 'Libri pasuria me e madhe ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam pretium risus sit amet est bibendum, non ullamcorper ipsum tristique. Vestibulum vestibulum sem eget ipsum finibus, vel commodo felis varius. Aliquam erat volutpat. Pellentesque efficitur, velit vel egestas auctor, odio ligula hendrerit ex, id sollicitudin mi ipsum at odio. Sed nec urna et odio bibendum congue. Nam non sapien vitae dolor bibendum rhoncus at ac velit. Vivamus euismod ut orci nec dictum. Morbi volutpat id turpis nec sollicitudin. Suspendisse potenti. Nulla facilisi. Duis quis tortor eget libero aliquet finibus. Nullam et urna et ipsum tristique cursus non sit amet quam.\r\n\r\nSed cursus nec libero vel rhoncus. Nam eleifend eget ligula nec sollicitudin. Cras tristique, erat at pharetra luctus, dolor velit cursus enim, nec vestibulum sapien elit eget sapien. Vivamus luctus libero eu odio condimentum, ac luctus ex feugiat. Curabitur vel tortor tristique, viverra urna at, elementum nunc. Suspendisse in elit id ante tristique vehicula. In vitae est nec libero feugiat blandit. Ut eget turpis ligula.\r\n\r\nInteger euismod interdum justo, sit amet dignissim odio tempus at. Vivamus luctus tincidunt lorem, ut posuere metus lacinia a. Integer ultrices quam et bibendum convallis. Nullam aliquam semper risus, eget efficitur orci malesuada quis. Maecenas auctor vel turpis eu gravida. Etiam euismod nunc quis ipsum tempus, id tristique metus volutpat. Sed euismod dolor eu urna blandit dictum. Sed semper vitae purus quis venenatis. Proin et velit nec sapien congue feugiat in vel dui. Sed euismod sapien a risus eleifend, a posuere ipsum fermentum.\r\n\r\nPellentesque bibendum lectus a lectus ullamcorper, a suscipit libero vulputate. Nulla facilisi. Fusce in arcu eget neque egestas scelerisque. Nam eget auctor quam. In luctus nisl eget libero ultrices cursus. Suspendisse ut justo nec massa volutpat mattis. Sed eget libero ut ligula suscipit dignissim. Nullam fermentum lectus quis lectus lacinia, id vehicula tellus lacinia. Sed hendrerit dolor id velit vulputate, nec vulputate lorem auctor. Maecenas accumsan turpis a nunc pharetra, nec posuere libero scelerisque.\r\n\r\nDuis suscipit varius tellus, eget feugiat justo. Etiam efficitur sodales est. Donec laoreet turpis at justo elementum, a dignissim ligula congue. Nulla facilisi. Suspendisse potenti. Morbi consectetur sollicitudin metus, vel dapibus velit mattis a. Fusce volutpat justo vel eros eleifend, et iaculis risus lacinia. Donec ut dolor in enim tincidunt euismod vel ac lectus. In eleifend tincidunt vestibulum. Maecenas malesuada nisl sit amet tellus iaculis, non tincidunt purus tincidunt.\r\n\r\nPhasellus feugiat eleifend justo, a viverra ante tincidunt eu. Pellentesque iaculis turpis vitae odio tempus, vel consectetur lorem sodales. Integer eu purus eleifend, semper neque ut, hendrerit ante. Vivamus non massa quis metus aliquet fringilla. Fusce malesuada id metus in vehicula. Phasellus aliquet lorem ut enim condimentum dignissim. Ut in hendrerit nulla. Fusce fermentum euismod sem, eget maximus sem gravida sed. Nunc viverra eros quis ex vulputate, eu euismod eros consectetur. Vivamus non ex vel libero lacinia scelerisque.\r\n\r\n', '2023-09-03 18:31:31', 17, 'blog_pictures/grid-img-7.jpg'),
(20, 'Makeup room that looks like a gallery', 'dhomat e makeupit ne itali duken sikur te jene nga e ardhmja', '2023-09-06 19:16:27', 1, 'blog_pictures/timestamp3.jpeg'),
(21, 'test per video ', 'edhe ky eshte nje test per testtesttesttesttesttesttest', '2023-09-07 21:31:47', 8, 'blog_pictures/blog-img.png'),
(22, 'test per video u ndrru', 'test per video test per video test per video test per video\r\nvideo u ndrru', '2023-09-07 21:41:37', 8, 'blog_pictures/chance-2-2.jpg'),
(23, 'titulli per video', 'contenti i webfaqes contenti i webfaqes contenti i webfaqes ', '2023-09-07 21:58:27', 8, 'blog_pictures/chance-3.jpeg'),
(24, 'titulli per ', 'contenticontenticontenticontenticontenticontenticontenti', '2023-09-08 19:26:08', 8, 'blog_pictures/fotojaPerBllog.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `translated_document`
--

CREATE TABLE `translated_document` (
  `documentid` int(11) NOT NULL,
  `finished_doc` blob DEFAULT NULL,
  `submission_date` date DEFAULT NULL,
  `document_details` varchar(100) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `translatorid` int(11) DEFAULT NULL,
  `languageid` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `translated_document`
--

INSERT INTO `translated_document` (`documentid`, `finished_doc`, `submission_date`, `document_details`, `userid`, `translatorid`, `languageid`, `status`) VALUES
(1, 0x7375626d6974732f363463666266643433336364375f44657479726174204a732052696e69204d6973696e692e646f6378, '2023-08-06', 'test again', 8, 7, 1, 2),
(2, 0x7375626d6974732f363463666464363530386237615f44657479726174204a732052696e69204d6973696e692e646f6378, '2023-08-06', 'Dear blend i hope everything is ok now', 8, 11, 1, 2),
(3, 0x7375626d6974732f363466346434356630643339325f61646d696e5f766965775f636f6d706f6e656e74732e747874, '2023-09-03', 'qe bre djal a je i knaqt ', 17, 10, 3, 2),
(4, 0x7375626d6974732f363466363165353234303431385f61646d696e5f766965775f636f6d706f6e656e74732e747874, '2023-09-04', 'qe ta bona mir haver', 8, 7, 1, 2),
(11, 0x7375626d6974732f363466386339333738323765375f626f7373692e747874, '2023-09-06', 'dear blendi projekti eshte gati', 8, 11, 1, 2),
(12, 0x7375626d6974732f363466386366363135303938395f626f7373692e747874, '2023-09-06', 'dokumenti u rikthy me sukses me njofto nese gjithqka eshte ne rregull', 8, 8, 2, 2),
(18, 0x7375626d6974732f363466613438343235663438335f61646d696e5f766965775f636f6d706f6e656e74732e747874, '2023-09-08', 'dokumenti u poerfundua ME SUSKES', 8, 7, 1, 2),
(19, 0x7375626d6974732f363466623735636239323734375f61646d696e5f766965775f636f6d706f6e656e74732e747874, '2023-09-08', 'projekti u kry', 8, 7, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `translating_requests`
--

CREATE TABLE `translating_requests` (
  `documentid` int(11) NOT NULL,
  `document_file` blob DEFAULT NULL,
  `request_details` varchar(100) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `languageid` int(11) DEFAULT NULL,
  `translatorid` int(11) DEFAULT NULL,
  `submission_date` date DEFAULT NULL,
  `deadline` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `translating_requests`
--

INSERT INTO `translating_requests` (`documentid`, `document_file`, `request_details`, `userid`, `languageid`, `translatorid`, `submission_date`, `deadline`, `status`) VALUES
(3, 0x75706c6f6164732f363466346431393731363434375f61646d696e5f766965775f636f6d706f6e656e74732e747874, 'Haver sma ke bo qysh kom dasht', 17, 3, 10, '2023-09-03', '2023-09-20 23:59:00', 2),
(18, 0x75706c6f6164732f363466613437626133393464395f61646d696e5f766965775f636f6d706f6e656e74732e747874, 'test test', 8, 1, 7, '2023-09-07', '2023-09-15 23:59:00', 2),
(19, 0x75706c6f6164732f363466623735373836633730645f61646d696e5f766965775f636f6d706f6e656e74732e747874, 'test', 8, 1, 7, '2023-09-08', '2023-09-15 21:26:00', 2);

-- --------------------------------------------------------

--
-- Table structure for table `translators`
--

CREATE TABLE `translators` (
  `translatorid` int(11) NOT NULL,
  `languageid` int(11) DEFAULT NULL,
  `name` varchar(20) NOT NULL,
  `surname` varchar(20) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `userid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `translators`
--

INSERT INTO `translators` (`translatorid`, `languageid`, `name`, `surname`, `phone`, `email`, `userid`) VALUES
(7, 1, 'Liridona', 'Berisha', '049123456', 'liridona.berisha@example.com', 2),
(8, 2, 'Artan', 'Gashi', '045987654', 'artan.gashi@example.com', 3),
(9, 3, 'Flaka', 'Krasniqi', '049246813', 'flaka.krasniqi@example.com', 4),
(10, 3, 'Besim', 'Hoxha', '049135792', 'besim.hoxha@example.com', 5),
(11, 1, 'Rita', 'Avdija', '045802364', 'rita.avdija@example.com', 6),
(12, 2, 'Ardit', 'Krasniqi', '049572139', 'ardit.krasniqi@example.com', 7),
(23, 1, 'papi', 'takallat', '123212322', 'papiTakalla@gmail.com', 25);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userid` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `surname` varchar(50) NOT NULL,
  `birthdate` date DEFAULT NULL,
  `personalnr` bigint(20) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `password` varchar(30) DEFAULT NULL,
  `role` int(11) DEFAULT NULL,
  `subid` int(11) DEFAULT NULL,
  `translatorid` int(11) DEFAULT NULL,
  `profile_picture_path` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userid`, `name`, `surname`, `birthdate`, `personalnr`, `phone`, `email`, `password`, `role`, `subid`, `translatorid`, `profile_picture_path`) VALUES
(1, 'Rini', 'Misini', '2001-09-19', 1249217278, '045888777', 'test1@gmail.com', '123456', 2, NULL, NULL, 'user_profile_pictures/rini_surrati.png'),
(2, 'Liridona', 'Berisha', '1990-05-15', 1234567899, '049123456', 'liridona.berisha@example.com', 'gtrans', 1, NULL, 7, 'user_profile_pictures/6.avif'),
(3, 'Artan', 'Gashi', '1985-11-02', 987654321, '045987654', 'artan.gashi@example.com', 'gtrans', 1, NULL, 8, 'user_profile_pictures/1.jpg'),
(4, 'Flaka', 'Krasniqi', '1992-09-25', 2468135790, '049246813', 'flaka.krasniqi@example.com', 'gtrans', 1, NULL, 9, 'user_profile_pictures/4.avif'),
(5, 'Besim', 'Hoxha', '1988-03-10', 1357924680, '049135792', 'besim.hoxha@example.com', 'gtrans', 1, NULL, 10, 'user_profile_pictures/5.avif'),
(6, 'Rita', 'Avdija', '1994-07-18', 8023645971, '045802364', 'rita.avdija@example.com', 'gtrans', 1, NULL, 11, 'user_profile_pictures/2.avif'),
(7, 'Ardit', 'Krasniqi', '1991-12-08', 5721394860, '049572139', 'ardit.krasniqi@example.com', 'gtrans', 1, NULL, 12, 'user_profile_pictures/3.avif'),
(8, 'Blend ', 'Gashi', '1996-12-22', 1234432321, '045994292', 'blendg@gmail.com', '123456', 0, 1, NULL, 'user_profile_pictures/blendi.webp'),
(17, 'Profa', 'Avdiu', '1999-07-14', 12433344556, '048171232', 'burimbossi@gmail.com', '123456', 0, 1, NULL, 'user_profile_pictures/profa.webp');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`clientid`),
  ADD UNIQUE KEY `userid` (`userid`),
  ADD KEY `fk_clients_subscriptions` (`subid`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`languageid`);

--
-- Indexes for table `newsletter`
--
ALTER TABLE `newsletter`
  ADD PRIMARY KEY (`newsid`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`post_id`),
  ADD KEY `fk_blog_userid` (`userid`);

--
-- Indexes for table `translated_document`
--
ALTER TABLE `translated_document`
  ADD PRIMARY KEY (`documentid`),
  ADD KEY `fk_translated_document_userid` (`userid`),
  ADD KEY `fk_translated_document_translator` (`translatorid`),
  ADD KEY `fk_translated_document_language` (`languageid`);

--
-- Indexes for table `translating_requests`
--
ALTER TABLE `translating_requests`
  ADD PRIMARY KEY (`documentid`),
  ADD KEY `fk_constraint_user` (`userid`),
  ADD KEY `fk_constraint_lang` (`languageid`),
  ADD KEY `fk_constraint_trans` (`translatorid`);

--
-- Indexes for table `translators`
--
ALTER TABLE `translators`
  ADD PRIMARY KEY (`translatorid`),
  ADD KEY `languageid` (`languageid`),
  ADD KEY `fk_translators_users` (`userid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userid`),
  ADD KEY `fk_user_subscription` (`subid`),
  ADD KEY `fk_translatorid_users` (`translatorid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `clientid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `languageid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `newsletter`
--
ALTER TABLE `newsletter`
  MODIFY `newsid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `post_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `translated_document`
--
ALTER TABLE `translated_document`
  MODIFY `documentid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `translators`
--
ALTER TABLE `translators`
  MODIFY `translatorid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `clients`
--
ALTER TABLE `clients`
  ADD CONSTRAINT `fk_userid_us` FOREIGN KEY (`userid`) REFERENCES `users` (`userid`) ON DELETE CASCADE;

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `fk_blog_userid` FOREIGN KEY (`userid`) REFERENCES `users` (`userid`);

--
-- Constraints for table `translated_document`
--
ALTER TABLE `translated_document`
  ADD CONSTRAINT `fk_translated_document_language` FOREIGN KEY (`languageid`) REFERENCES `language` (`languageid`),
  ADD CONSTRAINT `fk_translated_document_translator` FOREIGN KEY (`translatorid`) REFERENCES `translators` (`translatorid`),
  ADD CONSTRAINT `fk_translated_document_userid` FOREIGN KEY (`userid`) REFERENCES `users` (`userid`);

--
-- Constraints for table `translating_requests`
--
ALTER TABLE `translating_requests`
  ADD CONSTRAINT `fk_constraint_lang` FOREIGN KEY (`languageid`) REFERENCES `language` (`languageid`),
  ADD CONSTRAINT `fk_constraint_trans` FOREIGN KEY (`translatorid`) REFERENCES `translators` (`translatorid`),
  ADD CONSTRAINT `fk_constraint_user` FOREIGN KEY (`userid`) REFERENCES `users` (`userid`),
  ADD CONSTRAINT `translating_requests_ibfk_1` FOREIGN KEY (`documentid`) REFERENCES `translated_document` (`documentid`);

--
-- Constraints for table `translators`
--
ALTER TABLE `translators`
  ADD CONSTRAINT `translators_ibfk_1` FOREIGN KEY (`languageid`) REFERENCES `language` (`languageid`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_translatorid_users` FOREIGN KEY (`translatorid`) REFERENCES `translators` (`translatorid`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
