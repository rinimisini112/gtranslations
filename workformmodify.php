<?php 
include "inc/headerblank.php";
?>
<head>
    <link rel="stylesheet" href="css/workform.css">
    <style>
        #nav-logo ul li:nth-child(3) {
    border-bottom: 4px solid white;
    border-radius: 8px;
  }
  #nav-logo ul li:nth-child(4) {
    border-bottom:   unset;
    border-radius: unset;
  }
    </style>
</head>

<?php
$userid = $_SESSION['userid'];
if(isset($_GET['id'])) {
  $documentId = $_GET['id'];
$documentData = fetchDocumentData($documentId);

$languageId = $documentData['languageid'];
$requestId = $documentData['documentid'];
$languageName = $documentData['language'];
$docDetails = $documentData['request_details'];
$translatorName = $documentData['name'] . ' ' . $documentData['surname'];
$deadline = $documentData['deadline'];
}
?>

<section class="file-progress">
<h1 class='progress-title'>Update your project - Document.</h1>
    <form action="" method="post" id="modifyRequest">
        <label class='request-id-modify'>Your documents ID is: <?php echo $requestId ?> </label>
        <fieldset>
        <label for="selectLang">Change language </label><select name="selectLang" id="selectLang">
        <?php
                    echo "<option value='$languageId'>$languageName</option>";
                    $languages=getAllLanguages();
                    while ($language=mysqli_fetch_assoc($languages)) {
                        $langId=$language['languageid'];
                        $langName=$language['language'];
                        if($languageId!=$langId){
                            echo "<option value='$langId'>$langName</option>";
                        }
                        
                    }
                    
                    ?>
        </select>
        </fieldset>
        <fieldset>
        <label for="details">Add more Details</label>
        <textarea name="details" id="details"><?php if(!empty($docDetails)) echo $docDetails; ?></textarea>
        </fieldset>
        <p class='document-status'>Your document has been assigned to Translator: <?php echo $translatorName ?></p>
        <fieldset>
        <label for="deadline">Change Deadline</label>
        <input type="datetime-local" name="deadline" id="deadline" value="<?php if(!empty($deadline)) echo $deadline; ?>">
        </fieldset>
        <input type="submit" value="Save" name="modify">
        <?php 
        if(isset($_POST['modify'])) {
                        modifyTranslatingRequest($_POST['selectLang'],$_POST['details'],$_POST['deadline']);
                        echo "<div class='message' id='closePopUp'><p>Request modified successfully!</p>";
                                echo "<button class='closeMesagge'><a href='workformprogress.php'>Back.</a></button>";
                                echo "</div>";
                    }
                    ?>
    </form>
    </section>
    <script>

</script>