<?php 
include "inc/headerblank.php";
?>
<head>
    <link rel="stylesheet" href="css/profile.css">
</head>
<section class="myaccount">
<p class="sidebar-title">My Account</p>
<ul class="user-misc">

    <?php
generateProfileNavigation();
$userid = $_SESSION['userid'];
$user = userData($userid);

?>
</ul>
</section>
<section class="accountinfo">
    <p class="detail-title">My Profile <span><a href="editprofile.php">Edit</a></span></p>
    <div class="profilepicture">
        <?php 
        if(!$user['profile_picture_path']) {
            echo "<img src='images/default_no_profile.avif' alt='' class='profile-picture'>";
        } else {
            echo "<img src='{$user['profile_picture_path']}' alt='' class='profile-picture'>";
        }
        ?>
    </div>
    <div class="user-details">
        <?php 
        $name = $user['name'];
        $surname = $user['surname'];
        $birthdate = $user['birthdate'];
        $personalnr = $user['personalnr'];
        $phone = $user['phone'];
        $email = $user['email'];
        $password = $user['password'];
        $transID = $user['translatorid']
        ?>
    <form method="POST">
                <div class="form-row">
                  <label for="name">Name :</label>
                  <input disabled type="text" name="name" value="<?php if(!empty($name)) echo $name; ?>"/>
                  </div>
                  <div class="form-row">
                  <label for="surname">Surname :</label>
                  <input disabled type="text" name="surname" value="<?php if(!empty($surname)) echo $surname; ?>"/>
               </div>
                <div class="form-row">
                  <label for="birthdate">Date Of Birth :</label>
                  <input disabled type="date" name="birthdate" value="<?php if(!empty($birthdate)) echo $birthdate; ?>"/>
               </div>
                <div class="form-row">
                <label for="personalnr">Personal Number :</label>
                <input disabled type="tel" name="personalnr" id="personalnr" value="<?php if(!empty($personalnr)) echo $personalnr; ?>"/>
                </div>
          <div class="form-row">
                <label for="phone">Phone Number :</label>
                <input disabled type="tel" name="phone" id="phone" value="<?php if(!empty($phone)) echo $phone; ?>"/>
                </div>
                <div class="form-row">
                  <label for="email">Email :</label>
                  <input disabled type="email" name="email" id="" value="<?php if(!empty($email)) echo $email; ?>"/>
                </div>
                <div class="form-row">
                <label for="password">Password :</label>
                <input disabled type="password" name="password" value="<?php if(!empty($password)) echo $password; ?>"/> 
          </div>
    </form>
    </div>
</section>
</header>