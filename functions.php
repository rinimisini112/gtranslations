<?php
//==funksionet per lidhje me databaz login dhe regjister======

session_start();

function loadEnv($file = '.env') {
    if (file_exists($file)) {
        $lines = file($file, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
        foreach ($lines as $line) {
            if (strpos(trim($line), '#') === 0) {
                continue;
            }
            list($key, $value) = explode('=', $line, 2);
            $_ENV[trim($key)] = trim($value);
            putenv("$key=" . trim($value));
        }
    }
}

loadEnv();

function dbConn() {
    global $dbconn;
    
    $db_host = getenv('DB_HOST');
    $db_user = getenv('DB_USER');
    $db_password = getenv('DB_PASSWORD');
    $db_name = getenv('DB_NAME');

    $dbconn = mysqli_connect($db_host, $db_user, $db_password, $db_name);

    if (!$dbconn) {
        die("Deshtoi lidhja me DB" . mysqli_error($dbconn));
    }
}

dbConn();
dbConn();
function register($name,$surname,$birthdate,$personalnr,$phone,$email,$password) {
    global $dbconn;
    $PersonalNr_check = "SELECT userid FROM users WHERE personalnr='$personalnr'";
    $personalnr_check_result = mysqli_query($dbconn, $PersonalNr_check);
    $existing_number = mysqli_fetch_assoc($personalnr_check_result);
    
    $email_check_sql = "SELECT userid FROM users WHERE email='$email'";
    $email_check_result = mysqli_query($dbconn, $email_check_sql);
    $existing_email = mysqli_fetch_assoc($email_check_result);

    if ($existing_email || $existing_number) {
        echo "Email or Personal number is already taken, Please use another one or Log in with your credentials.";
        return; 
    }
    $sql = "SELECT userid FROM users WHERE name='$name' AND surname='$surname' 
    AND birthdate='$birthdate' AND personalnr='$personalnr' AND phone='$phone'";
    $result = mysqli_query($dbconn, $sql);
    $reshti = mysqli_fetch_assoc($result);

    if ($reshti) {
        echo "<p class='sukses'>U are already registered, Log in with your credentials</p>";
    } else {
    $sql = "INSERT INTO users(name,surname,birthdate,personalnr,phone,email,password,role)";
    $sql .= " VALUES('$name','$surname','$birthdate','$personalnr','$phone','$email','$password',0)";
    $result = mysqli_query($dbconn, $sql);
    if ($result) {
        echo "<p class='sukses'>Registration succesful. Log in with your credentials</p>";
    } else {
        die(mysqli_error($dbconn));
    }
} }
function login($email, $password)
{
    global $dbconn;
    $sql = "SELECT * FROM users";
    $sql .= " WHERE email='$email' AND password='$password'";
    $result = mysqli_query($dbconn, $sql);

    if (mysqli_num_rows($result) == 1) {
        $userData = mysqli_fetch_assoc($result);
        $_SESSION['userid'] = $userData['userid'];
        $_SESSION['subid'] = $userData['subid'];
        $_SESSION['role'] = $userData['role'];
        $user = array();
        $user['userid'] = $userData['userid'];
        $user['namesurname'] = $userData['name'] . " " . $userData['surname'];
        $user['role'] = $userData['role'];
        $translatorId = getTranslatorIdByUserId($userData['userid']);
        $_SESSION['translatorid'] = $translatorId;


        $_SESSION['user'] = $user;
        header("Location:index.php");
    } else {
        echo "<p class='login-error'>Your credentials are incorrect or user does not exist</p>";
    }
}
//======funksionet per te marrjen e te dhenave per perkthys me id=======
function getTranslatorIdByUserId($userId) {
    global $dbconn;
    $sql = "SELECT translatorid FROM translators WHERE userid = $userId LIMIT 1";
    $result = mysqli_query($dbconn, $sql);

    if ($result && mysqli_num_rows($result) > 0) {
        $row = mysqli_fetch_assoc($result);
        return $row['translatorid'];
    } else {
        return null;
    }
}
function getTranslatorDataByUserId($userId) {
    global $dbconn;
    $sql = "SELECT translatorid, languageid FROM translators WHERE userid = $userId";
    $result = mysqli_query($dbconn, $sql);

    if ($result && mysqli_num_rows($result) > 0) {
        $row = mysqli_fetch_assoc($result);
        return $row; 
    } else {
        return null; 
    }
}
 
            if (isset($_GET['argument']) && $_GET['argument'] == 'logout') {
                session_destroy();
                echo "index.php";
                exit;
            }
            
//funksionet per marrjen e te dhenave te userit dhe editimin e te dhenave======        
    function userData($userid)
{
    global $dbconn;
    $sql = "SELECT userid,name,surname,birthdate,profile_picture_path,personalnr,role,phone,email,password,translatorid FROM users WHERE userid=$userid";
    $result = mysqli_query($dbconn, $sql);
    if ($result) {
        return mysqli_fetch_assoc($result);
    } else {
        echo "User does not exist";
    }
}
function editProfile($userid,$name,$surname,$birthdate,$personalnr,$phone,$email,$password)
{
    global $dbconn;
    $sql = "UPDATE users SET name='$name', surname='$surname',birthdate='$birthdate',personalnr='$personalnr', phone='$phone' ";
    $sql .= " ,email='$email', password='$password' WHERE userid=$userid";
    $result = mysqli_query($dbconn, $sql);
    if ($result) {
        header("Location: profile.php");
        echo "Profile editet succesfully";
    } else {
        echo 'No changes were made';
        die(mysqli_error($dbconn));
    }
}
function updateProfilePicture($userId, $profilePicturePath)
{
    global $dbconn;
    $sql = "UPDATE users SET profile_picture_path = ? WHERE userid = ?";
    
    $stmt = mysqli_prepare($dbconn, $sql);
    
    if ($stmt) {
        mysqli_stmt_bind_param($stmt, "si", $profilePicturePath, $userId);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);
    }
}
//========funksionet per gjenerimin e navigacionet ne dy pjes te webfaqes========    
function generateNavigation() {
                global $dbconn;
                if (isset($_SESSION['user'])) {
                    if ($_SESSION['user']['role'] == 2) {
                        echo "<nav class='navbar-user'>";
                        echo "<div class='line_container'>";
                        echo "<a href='index.php' class='home'>Home</a>";
                        echo '<span></span>';
                        echo "</div>";
                        echo "<ul class='first_menu'>";
                        echo "<li><a href='staff.php'>Staff</a></li>";
                        echo "<li><a href='admin_dashboard.php'>All Docs</a></li>";
                        echo "<li><a href='blog.php'>Blog</a></li>";
                        echo "<li><a href='users_managment.php'>Users</a></li>";
                        echo "</ul>";
                        echo "<div class='line_container'>";
                        echo "<p class='user_sidebar'>User</p>";
                        echo '<span></span>';
                        echo "</div>";
                        echo "<ul>";
                        echo "<li><a href='profile.php'>Profile</a></li>";
                        echo "<li><a href='faq.php'>FAQ</a></li>";
                        echo "</ul>";
                        echo "<a href='#' id='logout'>Log Out</a>";
                        } else if ($_SESSION['user']['role'] == 1) { 
                        echo "<nav class='navbar-user'>";
                        echo "<div class='line_container'>";
                        echo "<a href='index.php' class='home'>Home</a>";
                        echo '<span></span>';
                        echo "</div>";
                        echo "<ul>";
                        echo "<li><a href='dashboard.php'>Dashboard</a></li>";
                        echo "<li><a href='submit_doc.php'>Submit</a></li>";
                        echo "<li><a href='blog.php'>Blog</a></li>";
                        echo "</ul>";
                        echo "<div class='line_container'>";
                        echo "<p class='user_sidebar'>User</p>";
                        echo '<span></span>';
                        echo "</div>";
                        echo "<ul>";
                        echo "<li><a href='profile.php'>Profile</a></li>";
                        echo "<li><a href='support.php'>Support</a></li>";
                        echo "<li><a href='faq.php'>FAQ</a></li>";
                        echo "</ul>";
                        echo "<a href='#' id='logout'>Log Out</a>";
                        } else if ($_SESSION['user']['role'] == 0) {
                            echo "<nav class='navbar-user'>";
                            echo "<div class='line_container'>";
                            echo "<a href='index.php' class='home'>Home</a>";
                            echo '<span></span>';
                            echo "</div>";
                            echo "<ul>";
                            echo "<li><a href='packages.php'>Packages</a></li>";
                            echo "<li><a href='blog.php'>Blog</a></li>";
                            echo "<li><a href='workform.php'>Contact Us</a></li>";
                            echo "<li><a href='view_all_docs.php'>Projects</a></li>";
                            echo "</ul>";
                            echo "<div class='line_container'>";
                            echo "<p class='user_sidebar'>User</p>";
                            echo '<span></span>';
                            echo "</div>";
                            echo "<ul>";
                            echo "<li><a href='profile.php'>Profile</a></li>";
                            echo "<li><a href='support.php'>Support</a></li>";
                            echo "<li><a href='faq.php'>FAQ</a></li>";
                            echo "</ul>";
                            echo "<a href='#' id='logout'>Log Out</a>";
                    }
                        } else  {
                            echo "<nav class='navbar'>";
                            echo "<ul class='first_menu'>";
                            echo "<li><a href='index.php'>Home</a></li>";
                            echo "<li><a href='services.php'>Our Services</a></li>";
                            echo "<li><a href='aboutus.php'>About Us</a></li>";
                            echo "</ul>";
                            echo "<a href='login-index.php' class='login_button'>Login</a>";
                        }
            }

            function generateProfileNavigation() {
                global $dbconn;
                if(isset($_SESSION['user'])) {
                    if(isset($_SESSION['user']['role']) && $_SESSION['user']['role'] == 2){
                        echo "<li><a href='profile.php'>Your Account<i class='bx bx-chevron-right-square'></i></a></li>";
                        echo "<li><a href='clientlist.php'>Clients<i class='bx bx-chevron-right-square'></i></a></li>";
                        echo "<li><a href='servicebuyers.php'>Applications<i class='bx bx-chevron-right-square'></i></a></li>";
                    } else if(isset($_SESSION['user']['role']) && $_SESSION['user']['role'] == 1){
                        echo "<li><a href='profile.php'>Your Account<i class='bx bx-chevron-right-square'></i></a></li>";
                    } else {
                        echo "<li><a href='profile.php'>Your Account<i class='bx bx-chevron-right-square'></i></a></li>";
                        echo "<li><a href='mysubscription.php'>Your Subscriptions<i class='bx bx-chevron-right-square'></i></a></li>";
                    }}
            }
            function getSubIDForBlog($userId) {
                global $dbconn;
                $userId = $_SESSION['userid'];
                $sql = "SELECT subid, role FROM users WHERE userid=$userId";
                $result = mysqli_query($dbconn,$sql);
                if($result) {
                    return mysqli_fetch_assoc($result);
                } else {
                    die(mysqli_error($dbconn));
                }
            }
            function generateBlogMenu() {
                global $dbconn;
                $userId = $_SESSION['userid'];
                $roleAndSub = getSubIDForBlog($userId);
                $userRole =  $roleAndSub['role'];
                $subId = $roleAndSub['subid'];
if(isset($subId) || isset($roleAndSub)) {
            if($subId > 0 || $userRole == 1 ||$userRole == 2) {
                echo "<li><a href='insert_blog.php'>Create Your own Blog post</a></li>";
                echo "<li><a href='view_user_posts.php'>View all your Posts</a></li>";
            }  else {
                echo "<li class='restricted'>Create Your own Blog post</li>";
                echo "<li class='restricted'>View all your Posts</li>";
            } } else {
                die(mysqli_error($dbconn));
            }
        }
//===========funksionet per newsletter aplikim dhe dergim te tij===========
            function newsletter($email) {
                global $dbconn;
                $sql = "SELECT newsid FROM newsletter WHERE email = '$email'";
            $result = mysqli_query($dbconn, $sql);
        
            if (mysqli_num_rows($result) == 0) {
              $sql = "INSERT INTO newsletter (email) VALUES ('$email')";
              mysqli_query($dbconn, $sql);
              echo "Subscription successful!";
            } else {
              echo "<p class='subscribe'>You are already subscribed!</p>";
            }
            }
            function sendNewsletter() {
    global $dbconn;
    $sql= "SELECT email FROM newsletter";
    $result = mysqli_query($dbconn,$sql);

    if ($result->num_rows > 0) {

        $sender_email = 'misinirini@gmail.com';

        $subject = 'Newsletter - Latest Updates';
        $message = 'Dear subscriber, thank you for subscribing stay tuned for more, xXx Gtranslations...';

        while ($row = mysqli_fetch_assoc($result)) {
            $subscriber_email = $row['email'];

            if (mail($subscriber_email, $subject, $message, "From: $sender_email")) {
            }
        }
    } else {
        echo "No subscribed emails found.";
    }

}
/*==========buy plans functions===========*/

function buyPlanMonthly($subid, $subType,$planName) {
    $userid = $_SESSION['userid'];
    global $dbconn;

    $checkQuery = "SELECT * FROM clients WHERE userid = $userid AND subid = $subid AND subscription_type = '$subType' AND plan_name='$planName'";
    $checkResult = mysqli_query($dbconn, $checkQuery);

    if (mysqli_num_rows($checkResult) > 0) {
        echo "<div class='buy-title'><p>You are already subscribed to this plan</p><i class='bx bx-x'></i></div>";
    } else { 
    $checkQuery = "SELECT * FROM clients WHERE userid = $userid";
    $checkResult = mysqli_query($dbconn, $checkQuery);

    if (mysqli_num_rows($checkResult) > 0) {
        echo "<div class='buy-title'>";
        echo "<form method='post' action='" . $_SERVER['PHP_SELF'] . "'>";
        echo "<p>Do you want to switch plans? <input type='submit' name='yes' id='switch' value='Switch'></p>";
        echo "<input type='hidden' name='plan-type' value='$subType'>";
        echo "<input type='hidden' name='plan-id' value='$subid'>";
        echo "<input type='hidden' name='plan-name' value='$planName'>";
        echo "<i class='bx bx-x'></i>";
        echo "</form>";
        echo "</div>";
    }else {
        $applicationDate = date('Y-m-d');
        $expirationDate = date('Y-m-d', strtotime('+30 days', strtotime($applicationDate)));
        $userQuery = "UPDATE users SET subid = $subid WHERE userid = $userid";
        $userResult = mysqli_query($dbconn, $userQuery);

        $sql = "INSERT INTO clients (subscription_type, application_date, expiration_date, userid, subid, plan_name) ";
        $sql .= "VALUES ('$subType', '$applicationDate', '$expirationDate', $userid, $subid,'$planName')";
        $result = mysqli_query($dbconn, $sql);

        if ($result && $userResult) {

            $_SESSION['subscription_type'] = $subType;
            $_SESSION['subid'] = $subid;
            $_SESSION['application_date'] = $applicationDate;
            $_SESSION['expiration_date'] = $expirationDate;   
            $_SESSION['plan_name'] = $planName;
 

            echo "<div class='buy-title'><p>Purchase successful! Your subscription has been activated</p><i class='bx bx-x'></i></div>";
        } else {
            echo "Error: " . mysqli_error($dbconn);
        }
    }
}
}

function buyPlanYearly($subid, $subType,$planName) {
    $userid = $_SESSION['userid'];
    global $dbconn;

    $checkQuery = "SELECT * FROM clients WHERE userid = $userid AND subid = $subid AND subscription_type = '$subType' AND plan_name='$planName'";
    $checkResult = mysqli_query($dbconn, $checkQuery);

    if (mysqli_num_rows($checkResult) > 0) {
        echo "<div class='buy-title'><p>You are already subscribed to this plan</p><i class='bx bx-x'></i></div>";
    } else { 
    $checkQuery = "SELECT * FROM clients WHERE userid = $userid";
    $checkResult = mysqli_query($dbconn, $checkQuery);

    if (mysqli_num_rows($checkResult) > 0) {
        echo "<div class='buy-title'>";
        echo "<form method='post' action='" . $_SERVER['PHP_SELF'] . "'>";
        echo "<p>Do you want to switch plans? <input type='submit' name='yes' id='switch' value='Switch'></p>";
        echo "<input type='hidden' name='plan-type' value='$subType'>";
        echo "<input type='hidden' name='plan-id' value='$subid'>";
        echo "<input type='hidden' name='plan-name' value='$planName'>";
        echo "<i class='bx bx-x'></i>";
        echo "</form>";
        echo "</div>";
    }else {
        $applicationDate = date('Y-m-d');
        $expirationDate = date('Y-m-d', strtotime('+1 year', strtotime($applicationDate)));

        $userQuery = "UPDATE users SET subid = $subid WHERE userid = $userid";
        $userResult = mysqli_query($dbconn, $userQuery);

        $sql = "INSERT INTO clients (subscription_type, application_date, expiration_date, userid, subid,plan_name) ";
        $sql .= "VALUES ('$subType', '$applicationDate', '$expirationDate', $userid, $subid,'$planName')";
        $result = mysqli_query($dbconn, $sql);

        if ($result && $userResult) {

            $_SESSION['subscription_type'] = $subType;
            $_SESSION['subid'] = $subid;
            $_SESSION['application_date'] = $applicationDate;
            $_SESSION['expiration_date'] = $expirationDate;   
            $_SESSION['plan_name'] = $planName;
 

            echo "<div class='buy-title'><p>Purchase successful! Your subscription has been activated</p><i class='bx bx-x'></i></div>";
        } else {
            echo "Error: " . mysqli_error($dbconn);
        }
    }
}
} 
 
function switchPlan($subid, $subType,$planName) {
    $userid = $_SESSION['userid'];
    global $dbconn;
    $checkQuery = "SELECT subscription_type, expiration_date FROM clients WHERE userid = $userid";
    $checkResult = mysqli_query($dbconn, $checkQuery);
    $row = mysqli_fetch_assoc($checkResult);

    $currentSubscriptionType = $row['subscription_type'];
    $currentExpirationDate = $row['expiration_date'];

    if ($currentSubscriptionType === $subType) {
        $updateQuery = "UPDATE clients SET subid = $subid, subscription_type = '$subType',plan_name='$planName' WHERE userid = $userid";
    } else {
        if ($subType === "Monthly") {
            $newExpirationDate = date('Y-m-d', strtotime('+30 days'));
        } else {
            $newExpirationDate = date('Y-m-d', strtotime('+1 year', strtotime($currentExpirationDate)));

            if ($currentSubscriptionType === "Yearly") {
                $newExpirationDate = date('Y-m-d', strtotime('-1 year', strtotime($newExpirationDate)));
            }
        }

        $updateQuery = "UPDATE clients SET subid = $subid, subscription_type = '$subType', expiration_date = '$newExpirationDate',plan_name='$planName' WHERE userid = $userid";
    }

    $updateResult = mysqli_query($dbconn, $updateQuery);

    if ($updateResult) {
        echo "<div class='buy-title'><p>Plan switched successfully!</p><i class='bx bx-x'></i></div>";
    } else {
        echo "Error: " . mysqli_error($dbconn);
    }
}
function getPlanDetails() {
    global $dbconn;
              $userid = $_SESSION['userid'];
         $sql = "SELECT c.subscription_type, c.application_date, c.expiration_date, c.plan_name, c.userid ";
          $sql .= "FROM clients c INNER JOIN users u ON c.userid=u.userid ";
          $sql .= "WHERE c.userid=$userid";
          $result = mysqli_query($dbconn,$sql);
          if($result) {
          return mysqli_fetch_assoc($result);
} else {
    die(mysqli_error($dbconn));
}
}
function deleteClientsPlan($userid)
{
    global $dbconn;

    mysqli_begin_transaction($dbconn);

    $sql_clients = "DELETE FROM clients WHERE userid=$userid";
    $result_clients = mysqli_query($dbconn, $sql_clients);

    if ($result_clients) {
        $sql_users = "UPDATE users SET subid=NULL WHERE userid=$userid";
        $result_users = mysqli_query($dbconn, $sql_users);

        if ($result_users) {
            mysqli_commit($dbconn);
            unset($_SESSION['subid']);
            echo "<div class='buy-title'><p>Unsubscription successful!</p><i class='bx bx-x'></i></div>";
        } else {
            mysqli_rollback($dbconn);
            die(mysqli_error($dbconn));
        }
    } else {
        mysqli_rollback($dbconn);
        die(mysqli_error($dbconn));
    }
}
//funksionet per dergim,editimin,fshirje,pranim te documenteve nga ana userit dhe perkthysit
//==============per perkthyes===============

function getTranslatorsForLanguage($languageId) {
    global $dbconn;
    $sql = "SELECT translatorid FROM translators WHERE languageid = $languageId";
    $result = mysqli_query($dbconn, $sql);

    if ($result) {
        $translators = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $translators[] = $row['translatorid'];
        }
        return $translators;
    } else {
        echo "Failed to fetch translators for the language.";
        return array(); 
    }
}
function fetchDocumentTranslator($translatorId) {
    global $dbconn;
    $sql = "SELECT r.documentid,r.document_file, r.request_details, r.userid, r.languageid,r.status,r.submission_date,r.deadline, t.name ,t.surname , l.language FROM translating_requests r INNER JOIN translators t ";
    $sql .= " ON r.translatorid = t.translatorid INNER JOIN language l ON r.languageid = l.languageid WHERE r.translatorid = $translatorId";
    $result = mysqli_query($dbconn,$sql);
    if($result) {
        return mysqli_fetch_assoc($result);
    } else {
        die(mysqli_error($dbconn));
    }
}
function getTranslatingRequestsForTranslator($translatorid)
{   
    global $dbconn;
    $sql = "SELECT r.documentid,r.status, u.userid, u.name, u.surname, r.request_details, r.deadline FROM translating_requests r INNER JOIN users u ON r.userid = u.userid WHERE r.translatorid = $translatorid";
    $result = mysqli_query($dbconn, $sql);

    if ($result && mysqli_num_rows($result) > 0) {
        $requests = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $requests[] = $row;
        }
        return $requests;
    } else {
        return array(); 
    }
}
function insertTranslatedDocument($documentId, $userId, $languageId, $details, $filePath, $translatorId)
{
    global $dbconn;
    $submissionDate = date('Y-m-d');

    $sql = "UPDATE translated_document
            SET userid = $userId, languageid = $languageId, document_details = '$details',submission_date = '$submissionDate',finished_doc = '$filePath', translatorid = $translatorId,status = 0
            WHERE documentid = $documentId";

    $result = mysqli_query($dbconn, $sql);

    if ($result) {
        return true; 
    } else {
        die(mysqli_error($dbconn));
        return false;
    }
}
function getUsersForTranslator($translatorId) {
    global $dbconn;
    $sql = "SELECT u.userid, u.name, u.surname, tr.documentid
    FROM users u
    INNER JOIN translating_requests tr ON u.userid = tr.userid
    WHERE tr.translatorid = $translatorId";
        $result = mysqli_query($dbconn, $sql);

    $users = array();
    while ($user = mysqli_fetch_assoc($result)) {
        $users[] = $user;
    }

    return $users;
}
function getSubmitedDocForTranslator($translatorId)
{   
    global $dbconn;
    $sql = "SELECT r.documentid,u.userid, u.name, u.surname,r.status, r.document_details, r.submission_date FROM translated_document r INNER JOIN users u ON r.userid=.u.userid WHERE r.translatorid = $translatorId";
    $result = mysqli_query($dbconn, $sql);

    if ($result && mysqli_num_rows($result) > 0) {
        $requests = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $requests[] = $row;
        }
        return $requests;
    } else {
        return array(); 
    }
}
function getSpecificRequest($documentid) {
    global $dbconn; 
    $sql = "SELECT u.userid, r.documentid, u.name, u.surname,r.deadline, r.request_details,r.status, r.submission_date, r.languageid, l.language, r.document_file";
    $sql .= " FROM translating_requests r INNER JOIN users u ON r.userid = u.userid"; // Correct the space before the dot
    $sql .=  " INNER JOIN language l ON r.languageid = l.languageid WHERE r.documentid = $documentid";
    $result = mysqli_query($dbconn, $sql);

    if ($result) {
        return mysqli_fetch_assoc($result);
    } else {
        die(mysqli_error($dbconn));
    }
}
function getSpecificDocument($docid) {
    global $dbconn; 
    $sql = "SELECT u.userid, r.documentid, u.name, u.surname, r.status,r.document_details, r.submission_date, r.languageid, l.language, r.finished_doc";
    $sql .= " FROM translated_document r INNER JOIN users u ON r.userid = u.userid"; // Correct the space before the dot
    $sql .=  " INNER JOIN language l ON r.languageid = l.languageid WHERE r.documentid = $docid";
    $result = mysqli_query($dbconn, $sql);

    if ($result) {
        return mysqli_fetch_assoc($result);
    } else {
        die(mysqli_error($dbconn));
    }
}
//====================FOR USERS========================
function getAllLanguages() {
    global $dbconn;
    $sql = "SELECT languageid, language FROM language";
    $result = mysqli_query($dbconn, $sql);
    if ($result) {
        return $result;
    } else {
        echo "Failed to fetch languages";
        die(mysqli_error($dbconn));
    }
}
function insertTranslatingRequest($userId, $languageId, $details, $submissionDate, $deadline, $filePath, $translatorId)
{
    global $dbconn;
    $submissionDate = date('Y-m-d');
    
    $sql = "INSERT INTO translated_document (documentid) VALUES (NULL)";
    $result = mysqli_query($dbconn, $sql);

    if (!$result) {
        die(mysqli_error($dbconn));
    }

   
    $documentId = mysqli_insert_id($dbconn);

    $sql = "INSERT INTO translating_requests (userid, languageid, request_details, submission_date, deadline, document_file, translatorid, documentid,status)";
    $sql .= "VALUES ($userId, $languageId, '$details', '$submissionDate', '$deadline', '$filePath', $translatorId, $documentId,0)";

    $result = mysqli_query($dbconn, $sql);

    if ($result) {
        return true;
    } else {
       die(mysqli_error($dbconn));
        return false;
    }
}
function handleFileUpload($fieldName)
{   
    $uploadDir = "uploads/"; 
    $uploadedFile = $_FILES[$fieldName]['tmp_name'];
    $originalFileName = $_FILES[$fieldName]['name'];

    $uniqueFileName = uniqid() . '_' . $originalFileName;

    $targetPath = $uploadDir . $uniqueFileName;

    if (move_uploaded_file($uploadedFile, $targetPath)) {
        return $targetPath; 
    } else {
        return null; 
    }
}
function fetchAllDocumentData($userid) {
    global $dbconn;
    $sql = "SELECT r.documentid,r.document_file, r.request_details, r.userid, r.languageid,r.status,r.submission_date,r.deadline, t.name ,t.surname , l.language FROM translating_requests r INNER JOIN translators t ";
    $sql .= " ON r.translatorid = t.translatorid INNER JOIN language l ON r.languageid = l.languageid WHERE r.userid = $userid ORDER BY documentid DESC";
    $result = mysqli_query($dbconn, $sql);
    
    if ($result) {
        $data = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $data[] = $row;
        }
        return $data;
    } else {
        die(mysqli_error($dbconn));
    }
}
function fetchDocumentDataById($documentid) {
    global $dbconn;
    $sql = "SELECT r.documentid,r.document_file, r.request_details, r.userid, r.languageid,r.status,r.submission_date,r.deadline, t.name ,t.surname , l.language FROM translating_requests r INNER JOIN translators t ";
    $sql .= " ON r.translatorid = t.translatorid INNER JOIN language l ON r.languageid = l.languageid WHERE r.documentid = $documentid";
    $result = mysqli_query($dbconn, $sql);
    
    if ($result) {
        $data = mysqli_fetch_assoc($result);
        if (!$data) {
           
            return array();
        }
        return $data;
    } else {
        die(mysqli_error($dbconn));
    }
} 
function fetchDocumentData($documentid) {
    global $dbconn;
    $sql = "SELECT r.documentid,r.document_file, r.request_details, r.userid, r.languageid,r.status,r.submission_date,r.deadline, t.name ,t.surname , l.language FROM translating_requests r INNER JOIN translators t ";
    $sql .= " ON r.translatorid = t.translatorid INNER JOIN language l ON r.languageid = l.languageid WHERE r.documentid = $documentid";
    $result = mysqli_query($dbconn, $sql);
    
    if ($result) {
        $data = mysqli_fetch_assoc($result);
        if (!$data) {

            return array();
        }
        return $data;
    } else {
        die(mysqli_error($dbconn));
    }
}   

function modifyTranslatingRequest($requestid,$requestDetails,$deadline) {
global $dbconn;
$userId = $_SESSION['userid'];
$sql = "UPDATE translating_requests SET request_details='$requestDetails',deadline='$deadline'";
$sql .= " WHERE userid=$userId";
$result = mysqli_query($dbconn, $sql);
if ($result) {
    return true;
} else {
    die(mysqli_error($dbconn));
}
}

function getTranslatingRequestsForUsers($userid)
{   
    global $dbconn;
    $sql = "SELECT r.documentid, u.userid, u.name, u.surname, r.request_details, r.status,r.deadline FROM translating_requests r INNER JOIN users u ON r.userid = u.userid WHERE r.userid = $userid";
    $result = mysqli_query($dbconn, $sql);

    if ($result && mysqli_num_rows($result) > 0) {
        $requests = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $requests[] = $row;
        }
        return $requests;
    } else {
        return array(); 
    }
}

function outputDocumentLink($filename, $filedata)
{
    header('Content-Type: application/octet-stream');
    header("Content-Transfer-Encoding: Binary");
    header("Content-disposition: attachment; filename=\"" . $filename . "\"");
    echo $filedata;
    exit;
}

if (isset($documentData)) {
    $filename = $documentData['filename'];
    $filedata = $documentData['filedata'];

    echo "<a href='javascript:void(0);' onclick='downloadDocument(\"$filename\")'>Download Document</a>";
}
function handleSubmitUpload($fieldName)
{   
    $uploadDir = "submits/"; 
    $uploadedFile = $_FILES[$fieldName]['tmp_name'];
    $originalFileName = $_FILES[$fieldName]['name'];

   
    $uniqueFileName = uniqid() . '_' . $originalFileName;

    $targetPath = $uploadDir . $uniqueFileName;

    if (move_uploaded_file($uploadedFile, $targetPath)) {
        return $targetPath; 
    } else {
        return null; 
    }
}
function getAllSubmitedDocsForUser($userid) {
    global $dbconn; 
    $sql = "SELECT u.name, u.surname, r.userid,r.status, r.documentid ,r.document_details, r.submission_date,r.status, r.finished_doc ";
    $sql .= " FROM translated_document r INNER JOIN users u ON r.userid = u.userid WHERE r.userid= $userid";
    $result = mysqli_query($dbconn,$sql);
     if($result) {
        return $result;
     } else {
        die(mysqli_error($dbconn));
     }
}
function getSubmitedDocForUser($userid) {
    global $dbconn; 
    $sql = "SELECT u.name, u.surname, r.userid,r.status, r.documentid ,r.document_details, r.submission_date,r.status, r.finished_doc ";
    $sql .= " FROM translated_document r INNER JOIN users u ON r.userid = u.userid WHERE r.userid= $userid";
    $result = mysqli_query($dbconn,$sql);
     if($result) {
        return mysqli_fetch_assoc($result);
     } else {
        die(mysqli_error($dbconn));
     }
}
function acceptSubmitedDoc($documentid) {
    global $dbconn;

    $request = "UPDATE translated_document SET status = 2 WHERE documentid = $documentid";
    $finished = "UPDATE translating_requests SET status = 2 WHERE documentid = $documentid";
    $requestResult = mysqli_query($dbconn, $request);
    $finishedResult = mysqli_query($dbconn, $finished);
    
    $results = array(
        "requestResult" => $requestResult,
        "finishedResult" => $finishedResult
    );
    
    if ($requestResult && $finishedResult) {
        return $results;
    } else {
        die(mysqli_error($dbconn));
    }
}
function declineDocument($documentid) {
    global $dbconn;
    
    $request = "UPDATE translated_document SET status = 1 WHERE documentid = $documentid";
    $finished = "UPDATE translating_requests SET status = 1 WHERE documentid = $documentid";
    
    $requestResult = mysqli_query($dbconn, $request);
    $finishedResult = mysqli_query($dbconn, $finished);
    
    $results = array(
        "requestResult" => $requestResult,
        "finishedResult" => $finishedResult
    );
    
    if ($requestResult && $finishedResult) {
        return $results;
    } else {
        die(mysqli_error($dbconn));
    }
}
function resendTranslatingRequest($documentid,$requestDetails,$deadline) {
    global $dbconn;
    $submissionDate = date('Y-m-d');
    $sql = "UPDATE translating_requests SET submission_date='$submissionDate',request_details='$requestDetails',deadline='$deadline'";
    $sql .= " WHERE documentid=$documentid";
    $result = mysqli_query($dbconn, $sql);
    if ($result) {
        return true;
        } else {
        die(mysqli_error($dbconn));
    }
    }
    //================funksionet per blog ================
    function fetchBlogPosts() {
        global $dbconn;
    
        $sql = "SELECT p.post_id,p.picture_path, p.title, p.content, p.created_at, u.profile_picture_path,u.name, u.surname
                FROM posts p
                INNER JOIN users u ON p.userid = u.userid
                ORDER BY p.created_at DESC";
    
        $result = mysqli_query($dbconn, $sql);
    
        if ($result) {
            $posts = array();
    
            while ($row = mysqli_fetch_assoc($result)) {
                $posts[] = $row;
            }
    
            return $posts;
        } else {
            die(mysqli_error($dbconn));
        }
    }
    function fetch_4_BlogPosts() {
        global $dbconn;
    
        $sql = "SELECT p.post_id,p.picture_path, p.title, p.content, p.created_at, u.name, u.surname,u.profile_picture_path
                FROM posts p
                INNER JOIN users u ON p.userid = u.userid
                ORDER BY p.created_at DESC LIMIT 4";
    
        $result = mysqli_query($dbconn, $sql);
    
        if ($result) {
            $posts = array();
    
            while ($row = mysqli_fetch_assoc($result)) {
                $posts[] = $row;
            }
    
            return $posts;
        } else {
            die(mysqli_error($dbconn));
        }
    }
    function fetchBlogPostsById($userId) {
        global $dbconn;
    
        $sql = "SELECT p.post_id, p.picture_path, p.title, p.content, p.created_at, u.name, u.surname, u.profile_picture_path
        FROM posts p
        INNER JOIN users u ON p.userid = u.userid
        WHERE p.userid = $userId
        ORDER BY p.created_at DESC";
    
        $result = mysqli_query($dbconn, $sql);
    
        if ($result) {
            $posts = array();
    
            while ($row = mysqli_fetch_assoc($result)) {
                $posts[] = $row;
            }
    
            return $posts;
        } else {
            die(mysqli_error($dbconn));
        }
    }
    function fetchBlogPostByPostId($postId) {
        global $dbconn;
    
        $sql = "SELECT p.post_id, p.picture_path, p.title, p.content, p.created_at, u.name, u.surname,u.profile_picture_path
                FROM posts p
                INNER JOIN users u ON p.userid = u.userid
                WHERE p.post_id = $postId";
    
        $result = mysqli_query($dbconn, $sql);
    
        if ($result) {
            $post = mysqli_fetch_assoc($result); 
    
            return $post;
        } else {
            die(mysqli_error($dbconn));
        }
    }
    function viewBlogPostById($postid) {
        global $dbconn;
    
        $sql = "SELECT p.post_id, p.picture_path, p.title, p.content, p.created_at, u.name, u.surname,u.profile_picture_path
                FROM posts p
                INNER JOIN users u ON p.userid = u.userid
                WHERE post_id = $postid";
    
        $result = mysqli_query($dbconn, $sql);
    
        if ($result) {
            $row = mysqli_fetch_assoc($result);
            return $row; 
        } else {
            die(mysqli_error($dbconn));
        }
    }
    function editBlogPost($title, $content, $picture, $postId) {
        global $dbconn;
    
        $existingPicturePath = getExistingPicturePath($postId);
    
        if (empty($picture)) {
            $picture = $existingPicturePath;
        }
    
        $sql = "UPDATE posts SET title='$title', content='$content', picture_path='$picture' WHERE post_id=$postId";
        $result = mysqli_query($dbconn, $sql);
    
        if ($result) {
            return $result;
        } else {
            die(mysqli_error($dbconn));
        }
    }
    function getExistingPicturePath($postId) {
        global $dbconn;
    
        $sql = "SELECT picture_path FROM posts WHERE post_id = $postId";
        $result = mysqli_query($dbconn, $sql);
    
        if ($result && mysqli_num_rows($result) > 0) {
            $row = mysqli_fetch_assoc($result);
            return $row['picture_path'];
        } else {
            return '';
        }
    }
    function insertBlogPost($title, $content, $userId, $picture) {
        global $dbconn; 
        
        $sql = "INSERT INTO posts (title, content, userid, created_at, picture_path)
                VALUES ('$title', '$content', $userId, NOW(), '$picture')";
        
       
        $result = mysqli_query($dbconn,$sql); 
        if ($result) {
            return true;
        } else {
            die(mysqli_error($dbconn));
            return false; 
        }
    }
    function searchBlogPosts($searchQuery) {
        global $dbconn; 
    
        $escapedSearchQuery = mysqli_real_escape_string($dbconn, $searchQuery);
    

        $sql = "SELECT p.post_id, p.picture_path, p.title, p.content, p.created_at, u.name, u.surname,u.profile_picture_path
                FROM posts p
                INNER JOIN users u ON p.userid = u.userid
                WHERE p.title LIKE '%$escapedSearchQuery%' OR p.content LIKE '%$escapedSearchQuery%' OR CONCAT(u.name, ' ', u.surname) LIKE '%$escapedSearchQuery%'
                ORDER BY p.created_at DESC";
    
        $result = mysqli_query($dbconn, $sql);
    
        if ($result) {
            $searchResults = array();
    
            while ($row = mysqli_fetch_assoc($result)) {
                $searchResults[] = $row;
            }
    
            return $searchResults;
        } else {
            die(mysqli_error($dbconn));
        }
    }
    function selectUserForAdmin() {
        global $dbconn;
        $sql = "SELECT * FROM users";
        $result = mysqli_query($dbconn,$sql);
        if($result) {
            return $result;
        } else {
            die(mysqli_error($dbconn));
        }
    }
    function deleteBlogPost($postid) {
        global $dbconn;
        $sql = "DELETE FROM posts WHERE post_id = $postid";
        
        $result = mysqli_query($dbconn,$sql);
        
        if ($result) {
            return true;
        } else {
            die(mysqli_error($dbconn));
            return false;
        }
    }
    
    //========funksioni per shfaqjen e blogut ne footer=====
    function getLatestBlogPosts($limit = 2) {
        global $dbconn; 
    
        $sql = "SELECT * FROM posts ORDER BY created_at DESC LIMIT $limit";
        $result = mysqli_query($dbconn, $sql);
    
        if (!$result) {
            die(mysqli_error($dbconn)); // Handle the error appropriately
        }
    
        $blogPosts = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $blogPosts[] = $row;
        }
    
        return $blogPosts;
    }
    //========funksionet per admin===================
    function searchUsers($searchQuery) {
        global $dbconn; 
    
        $sql = "SELECT *
                FROM users
                WHERE personalnr LIKE ? OR birthdate LIKE ? OR CONCAT(name, ' ', surname) LIKE ? OR email LIKE ?";
    
        $stmt = mysqli_prepare($dbconn, $sql);
    
        if ($stmt) {
            $param = "%$searchQuery%"; // Assuming you want to search for partial matches
            mysqli_stmt_bind_param($stmt, "ssss", $param, $param, $param, $param);
    
            $result = mysqli_stmt_execute($stmt);
    
            if ($result) {
                $searchResults = array();
    
                $result_set = mysqli_stmt_get_result($stmt);
    
                while ($row = mysqli_fetch_assoc($result_set)) {
                    $searchResults[] = $row;
                }
    
                mysqli_stmt_close($stmt);
                return $searchResults;
            } else {
                die(mysqli_error($dbconn));
            }
        } else {
            die(mysqli_error($dbconn));
        }
    }
    function loadRoleForAdmin($role) {
        if ($role == 2) {
            echo "Admin";
        } else if ($role == 1) {
            echo "Translator";
        } else {
            echo "User";
        }
    }
    function editUserInfo($userid, $name, $surname, $birthdate, $personalnr, $phone, $email, $role, $password) {
        global $dbconn;
        
        $sql = "UPDATE users SET name='$name', surname='$surname', birthdate='$birthdate', personalnr='$personalnr',
                phone='$phone', email='$email', role='$role', password='$password' WHERE userid=$userid";
    
        if (mysqli_query($dbconn, $sql)) {
            return true;
        } else {
            return false;
        }
    }
    function deleteUserForAdmin($userid) {
        global $dbconn;
    
        $sql = "DELETE FROM users WHERE userid=$userid";
    
        $result = mysqli_query($dbconn, $sql);
        if($result) {
        return $result;
    }
}
function fetchTranslatorsForAdmin() {
    global $dbconn;
    $sql = "SELECT
        t.userid,
        t.name,
        t.surname,
        t.languageid,
        l.language,
        t.phone,
        t.email,
        u.profile_picture_path,
        t.translatorid
    FROM
        translators t
    INNER JOIN
        language l ON t.languageid = l.languageid
    INNER JOIN
        users u ON t.userid = u.userid
    LEFT JOIN
        translated_document fd ON t.translatorid = fd.translatorid
    GROUP BY
        t.translatorid;";

    $result = mysqli_query($dbconn, $sql);
    if ($result) {
       

        return $result; 
    } else {
        die(mysqli_error($dbconn));
    }
}

function fetchTranslatorForAdmin($translatorId) {
    global $dbconn;
    $sql = "SELECT
        t.userid,
        t.translatorid,
        t.name,
        t.surname ,
        t.languageid ,
        u.personalnr,
        u.birthdate,
        l.language ,
        t.phone ,
        t.email ,
        u.profile_picture_path, 
        fd.translatorid
    FROM
        translators t
    INNER JOIN
        language l ON t.languageid = l.languageid
    INNER JOIN
        users u ON t.userid = u.userid
    LEFT JOIN
        translated_document fd ON t.translatorid = fd.translatorid
    WHERE t.translatorid=$translatorId";
    
    $result = mysqli_query($dbconn, $sql);
    if ($result) {
            return $result; 
    } else {
        die(mysqli_error($dbconn));
    }
}

function displayTranslatedDocumentsForTranslator($translatorId) {
    global $dbconn;
    
    $sql = "SELECT 
    td.documentid,
    td.submission_date,
    td.status,
    u.name,
    u.surname,
    l.language
FROM 
    translated_document td
INNER JOIN 
    users u ON td.userid = u.userid
INNER JOIN 
    language l ON td.languageid = l.languageid
WHERE 
    td.translatorid = $translatorId";
    $result = mysqli_query($dbconn, $sql);
    
    if ($result) {
        return $result;
    } else {
        die(mysqli_error($dbconn));
    }
}

function registerTranslator($name, $surname, $birthdate, $personalnr, $phone, $email, $password) {
    global $dbconn;

    $PersonalNr_check = "SELECT userid FROM users WHERE personalnr='$personalnr'";
    $personalnr_check_result = mysqli_query($dbconn, $PersonalNr_check);
    $existing_number = mysqli_fetch_assoc($personalnr_check_result);

    $email_check_sql = "SELECT userid FROM users WHERE email='$email'";
    $email_check_result = mysqli_query($dbconn, $email_check_sql);
    $existing_email = mysqli_fetch_assoc($email_check_result);

    if ($existing_email || $existing_number) {
        echo "Email or Personal number is already taken. Please use another one or log in with your credentials.";
        return;
    }

    $sql = "INSERT INTO users(name, surname, birthdate, personalnr, phone, email, password, role)";
    $sql .= " VALUES('$name', '$surname', '$birthdate', '$personalnr', '$phone', '$email', '$password', 1)";

    $result = mysqli_query($dbconn, $sql);

    if (!$result) {
        die(mysqli_error($dbconn));
    }

    $userid = mysqli_insert_id($dbconn); 

    $translator_sql = "INSERT INTO translators(userid)";
    $translator_sql .= " VALUES($userid)";

    $translator_result = mysqli_query($dbconn, $translator_sql);

    if ($translator_result) {
        $update_sql = "UPDATE users SET translatorid = (SELECT translatorid FROM translators WHERE userid = $userid) WHERE userid = $userid";
        $update_result = mysqli_query($dbconn, $update_sql);

        if ($update_result) {
            $_SESSION['new_translator_userid'] = $userid;
            } else {
            die(mysqli_error($dbconn));
        }
    } else {
        die(mysqli_error($dbconn));
    }
}

function getTranslatorData($userId) {
    global $dbconn;
    $sql = "SELECT * FROM users WHERE userid=$userId";
    $result = mysqli_query($dbconn, $sql);
    if ($result) {
        return mysqli_fetch_assoc($result);
    } else {
        die(mysqli_error($dbconn));
    }
}
function insertTranslator($name, $surname, $phone, $email, $languageId,$userId) {
    global $dbconn;
    $sql = "UPDATE translators SET name='$name', surname='$surname', phone='$phone', email='$email', languageid='$languageId' WHERE userid=$userId";
    $result = mysqli_query($dbconn, $sql);
    if($result) {
        return $result;
    }
}
function deleteTranslator($staffId) {
    global $dbconn;

    $translatorsDeleteSql = "DELETE FROM translators WHERE translatorid = $staffId";
    $translatorsDeleteResult = mysqli_query($dbconn, $translatorsDeleteSql);

    if (!$translatorsDeleteResult) {
        die("Error deleting translator from translators table: " . mysqli_error($dbconn));
    }

    $usersDeleteSql = "DELETE FROM users WHERE userid = $staffId";
    $usersDeleteResult = mysqli_query($dbconn, $usersDeleteSql);

    if (!$usersDeleteResult) {
        die("Error deleting translator from users table: " . mysqli_error($dbconn));
    }

    echo "<p class='delete_succes'>Translator deleted successfully.<a href='staff.php'>Return to Staff page</a></p>";
}
function fetchRequestDataAdmin() {
    global $dbconn;
    $sql = "SELECT r.documentid,r.document_file, r.request_details, r.userid, r.languageid,r.status,r.submission_date,
    r.deadline, t.name ,t.surname , l.language,
    u.name AS clientName,u.surname AS clientSurname FROM translating_requests r INNER JOIN translators t ";
    $sql .= " ON r.translatorid = t.translatorid INNER JOIN language l ON r.languageid = l.languageid
     INNER JOIN users u on r.userid= u.userid";
    $result = mysqli_query($dbconn, $sql);
    
    if ($result) {
        $data = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $data[] = $row;
        }
        return $data;
    } else {
        die(mysqli_error($dbconn));
    }
}
function fetchDocumentDataAdmin() {
    global $dbconn;
    $sql = "SELECT r.documentid,r.finished_doc, r.document_details, r.userid, r.languageid,r.status,r.submission_date,
     t.name ,t.surname , l.language,
    u.name AS clientName,u.surname AS clientSurname FROM translated_document r INNER JOIN translators t ";
    $sql .= " ON r.translatorid = t.translatorid INNER JOIN language l ON r.languageid = l.languageid
     INNER JOIN users u on r.userid= u.userid";
    $result = mysqli_query($dbconn, $sql);
    
    if ($result) {
        $data = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $data[] = $row;
        }
        return $data;
    } else {
        die(mysqli_error($dbconn));
    }
}
function getTotalFinishedDocuments() {
    global $dbconn;
 
    $sql = "SELECT COUNT(*) AS total FROM translated_document WHERE status = 2"; 
    $result = mysqli_query($dbconn, $sql);

    if ($result) {
        $row = mysqli_fetch_assoc($result);
        return $row['total'];
    } else {
        die(mysqli_error($dbconn));
    }
}
function singleDocumentDataAdmin($docId) {
    global $dbconn;
    $sql = "SELECT r.documentid,r.finished_doc, r.document_details,profile_picture_path, r.userid, r.languageid,r.status,r.submission_date,
     t.name ,t.surname , l.language,r.translatorid,
    u.name AS clientName,u.surname AS clientSurname FROM translated_document r INNER JOIN translators t ";
    $sql .= " ON r.translatorid = t.translatorid INNER JOIN language l ON r.languageid = l.languageid
     INNER JOIN users u on r.userid= u.userid WHERE documentid = $docId";
    $result = mysqli_query($dbconn, $sql);
    
    if ($result) {
        $row = mysqli_fetch_assoc($result);
        return $row;
    } else {
        die(mysqli_error($dbconn));
    }
}
function singleRequestDataAdmin($docId) {
    global $dbconn;
    $sql = "SELECT r.documentid, r.document_file, r.request_details,profile_picture_path, r.userid, r.languageid, r.status, r.submission_date,
    r.deadline, t.name, t.surname,r.translatorid, l.language,
    u.name AS clientName, u.surname AS clientSurname FROM translating_requests r
    INNER JOIN translators t ON r.translatorid = t.translatorid
    INNER JOIN language l ON r.languageid = l.languageid
    INNER JOIN users u ON r.userid = u.userid
    WHERE documentid = $docId
    LIMIT 1";
    $result = mysqli_query($dbconn, $sql);
    
    if ($result) {
        $row = mysqli_fetch_assoc($result);
        return $row; 
    } else {
        die(mysqli_error($dbconn));
    }
}
function getTranslatorPicture($translatorid) {
    global $dbconn;
    $sql = "SELECT * FROM users WHERE translatorid=$translatorid";
    $result = mysqli_query($dbconn,$sql);
    if($result) {
        return mysqli_fetch_assoc($result);
    } else {
        die(mysqli_error($dbconn));
    }
}



