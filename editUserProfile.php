<?php
include "inc/headerblank.php";
if (isset($_GET['id'])) {
    $userid = $_GET['id'];
    $user = userData($userid);
    $name = $user['name'];
    $surname = $user['surname'];
?>
    <style>
       .sidebar .first_menu li:nth-child(4) a  {
            border-bottom: 2px solid white;
        }
    </style>
    <head>
        <link rel="stylesheet" href="css/admin.css">
    </head>
    <section class="accountinfo">
        <div class="detail-title"><p>Viewing :
            <?php
            echo "$name" . " " . "$surname";
            ?></p> 
            <div class="crud_butons">
                 <a href="edit_users.php?id=<?php echo  $userid?>">Back</a>
                 <form method="post" id="registerTranslator">
                    <?php 
                    if (isset($_POST['deleteUser'])) {
                        deleteUserForAdmin($userid);
                        header("Location:users_management.php");
                    }
                    ?>
                    <input type="submit" name="deleteUser" value='Delete'>
                </form>
            </div>
        </div>
        <div class="profilepicture">
            <?php
             $profilePicturePath = $user['profile_picture_path'];
             if(!$profilePicturePath) {
                echo "<img src='images/default_no_profile.avif' alt='' class='profile-picture'>";
             } else {
                echo "<img src='$profilePicturePath' alt='Profile Picture' class='profile-pic-mini'>";
             }
             ?>
        </div>
        <div class="user-details">
        <?php
        $birthdate = $user['birthdate'];
        $personalnr = $user['personalnr'];
        $phone = $user['phone'];
        $email = $user['email'];
        $role = $user['role'];
        $transID = $user['translatorid'];
        $password = $user['password'];

    }
        ?>
       <form method="POST">
    <div class="form-grid">
        <div class="form-column">
        <div class="form-row">
    <label for="Role">Role :</label>
    <select name="role" id="role">
        <?php
        $roles = array(
            0 => "User",
            1 => "Translator",
            2 => "Admin"
        );

        // Loop through the roles array to generate options
        foreach ($roles as $roleId => $roleName) {
            // Check if the current role ID matches the user's role
            $selected = ($role == $roleId) ? "selected" : "";

            // Output the option element
            echo "<option value='$roleId' $selected>$roleName</option>";
        }
        ?>
    </select>
</div>
            <div class="form-row">
                <label for="name">Name :</label>
                <input type="text" name="name" id="name" value="<?php if (!empty($name)) echo $name; ?>" />
            </div>
            <div class="form-row">
                <label for="surname">Surname :</label>
                <input type="text" name="surname" id="surname" value="<?php if (!empty($surname)) echo $surname; ?>" />
            </div>
            <div class="form-row">
                <label for="birthdate">Date Of Birth :</label>
                <input type="date" name="birthdate" id="birthdate" value="<?php if (!empty($birthdate)) echo $birthdate; ?>" />
            </div>
        </div>
        <div class="form-column">
            <div class="form-row">
                <label for="personalnr">Personal Number :</label>
                <input type="tel" name="personalnr" id="personalnr" value="<?php if (!empty($personalnr)) echo $personalnr; ?>" />
            </div>
            <div class="form-row">
                <label for="phone">Phone Number :</label>
                <input type="tel" name="phone" id="phone" value="<?php if (!empty($phone)) echo $phone; ?>" />
            </div>
            <div class="form-row">
                <label for="email">Email :</label>
                <input type="email" name="email" id="email" value="<?php if (!empty($email)) echo $email; ?>" />
            </div>
            <div class="form-row">
                <label for="password">Password :</label>
                <input type="text" name="password" id="password" value="<?php if (!empty($password)) echo $password; ?>" />
            </div>
            <div class="form-row">
                <input type="submit" name="editInfo" id="editInfo" value="Save" />
            </div>
        </div>
    </div>
</form>
<?php 
if(isset($_POST['editInfo'])) {
    $editedName = $_POST['name'];
    $editedSurname = $_POST['surname'];
    $editedBirthdate = $_POST['birthdate'];
    $editedPersonalnr = $_POST['personalnr'];
    $editedPhone = $_POST['phone'];
    $editedEmail = $_POST['email'];
    $editedRole = $_POST['role'];
    $editedPassword = $_POST['password'];

    if (editUserInfo($userid, $editedName, $editedSurname, $editedBirthdate, $editedPersonalnr, $editedPhone, $editedEmail, $editedRole, $editedPassword)) {
        echo "<p class='user_edited_check'>User edited succesfully <a href='edit_users.php?id=$userid'>Go Back</a></p>";
    }
} 
?>
        </div>
    </section>
    </header>