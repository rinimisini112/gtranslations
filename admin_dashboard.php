<?php 
include "inc/headerblank.php";
$totalFinishedDocs = getTotalFinishedDocuments();
$totalFinishedDocsFormatted = str_pad($totalFinishedDocs, 2, '0', STR_PAD_LEFT); // Format the total as double digits
?>
<head>
    <link rel="stylesheet" href="css/admin_dashboard.css">
    <style>
        body {
            overflow-y: unset;
        }
        .sidebar .first_menu li:nth-child(2) a  {
            border-bottom: 2px solid white;
        }
        #header-container {
            display: flex;
      }
    </style>
</head>
<h1 class="intro_title">View all documents progress, status, and details.</h1>
<div class="info_subtitle">
    <h2>Documents that have the same 'Doc ID' are connected, meaning they have the same Client and assigned Translator in both of them.</h2>
    <span class="counter_text">Total Document counter :</span>
    <span class="double_digit_counter"><?php echo $totalFinishedDocsFormatted; ?></span>
</div>
<section class="doc-progress">
    <h1 class="progress-title">Document Requests - Sent by Clients</h1>
    <table>
        <thead>
            <tr>
            <th>Doc ID</th>                
                <th>Client Name</th>                
                <th>Language</th>
                <th>Deadline</th>
                <th>View</th>
            </tr>
        </thead>
        <tbody>
            <?php   
            $translatingRequests = fetchRequestDataAdmin();

            foreach ($translatingRequests as $request) {
                $TranslatorName = $request['name'] . ' ' . $request['surname']; 
                $userName = $request['clientName'] . ' ' . $request['clientSurname']; 
                $language = $request['language'];
                $deadline = $request['deadline'];
                $projectId = $request['documentid'];
                $docUserId = $request['userid'];
                $_SESSION['docId'] = $docUserId;

            ?>
            <tr>
                <?php if($request['status'] == 1) {
                    echo "<td class='white'>Re sent!</td>";
                } else if ($request['status']== 0){
                    echo "<td>...</td>";
                    }?>
                <td><?php echo $projectId; ?></td>
                <td><?php echo $userName; ?></td>
                <td><?php echo $language; ?></td>
                <td><?php echo $deadline; ?></td>
                <td><a href="view_document_details_admin.php?rid=<?php echo $projectId?>"><i class="fa-regular fa-eye"></i></a></td>
                           </tr>
            <?php
            }
            ?>
        </tbody>
    </table>
</section>
<section class="doc-progress">
    <h1 class="progress-title">Finished Documents - Sent by Translators</h1>
    <table>
        <thead>
            <tr>
            <th>Doc ID</th>                
                <th>Translator Name</th>
                <th>Language</th>
                <th>Submission Date</th>
                <th>Status</th>
                <th>View</th>
            </tr>
        </thead>
        <tbody>
            <?php   
            $translatingRequests = fetchDocumentDataAdmin();

            foreach ($translatingRequests as $request) {
                $TranslatorName = $request['name'] . ' ' . $request['surname']; 
                $userName = $request['clientName'] . ' ' . $request['clientSurname']; 
                $language = $request['language'];
                $deadline = $request['submission_date'];
                $projectId = $request['documentid'];
                $docUserId = $request['userid'];
                $_SESSION['docId'] = $docUserId;
            ?>
            <tr>
                <?php if($request['status'] == 1) {
                    echo "<td class='white'>Re sent!</td>";
                } else if ($request['status']== 0){
                    echo "<td>...</td>";
                    }?>
                <td><?php echo $projectId; ?></td>
                <td><?php echo $TranslatorName; ?></td>
                <td><?php echo $language; ?></td>
                <td><?php echo $deadline; ?></td>
                <td>
                        <?php if ($request['status'] == 2): ?>
                            Finished
                        <?php elseif ($request['status'] == 1): ?>
                            <a href='view_request.php?id=<?php echo $request['documentid']; ?>'>Declined,Priority!</a>
                            <?php else: ?>
                            <a href='view_request.php?id=<?php echo $request['documentid']; ?>'>View Details</a>
                        <?php endif; ?>
                    </td>              
                    <td><a href="view_document_details_admin.php?did=<?php echo $projectId?>"><i class="fa-regular fa-eye"></i></a></td>
 </tr>
            <?php
            }
            ?>
        </tbody>
    </table>
</section>
</section>