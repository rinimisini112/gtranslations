<?php 
include "inc/headerblank.php";
?>
<head>
    <link rel="stylesheet" href="css/blog.css">
    <style>
        .sidebar .first_menu li:nth-child(3) a  {
            border-bottom: 2px solid white;
        }
        body {
            overflow-y: unset;
        }
        .deleteBlogUser {
    margin: unset;
}
    </style>
</head>
<section class="blog-container">
    <nav class="blog_menu">
        <ul>
            <li><a href="blog.php">View All Posts</a></li>
            <li><a href="insert_blog.php">Create Your own Blog post</a></li>
            <li><a href="view_user_posts.php">View all your Posts</a></li>
            <form method="GET" action="search_results.php">
    <input type="text" class='queryInput' name="query" placeholder="Search...">
    <button type="submit">Search <i class='bx bx-search-alt' ></i></button>
</form>
        </ul>
    </nav>
    <h1 class="blog_introduction">The Blog, view and edit your posts.</h1>
    <div class="post-container">
        <p class="all_posts_title">Your posts</p>
        <?php

        
        $blogPosts = fetchBlogPostsById($_SESSION['userid']);
    if(!$blogPosts) {
        echo "<p class='noPosts'>You have not posted yet anything yet!</p>";
    } else {
        foreach ($blogPosts as $post) {
            echo "<div class='blog-post'>";
            echo "<h3 class='post_number'>Viewing post NR.{$post['post_id']}</h2>";
            if ($post['picture_path'] !== null) {
                echo "<img class='blog_banner' src='{$post['picture_path']}' alt='Blog Image'>";
            } else {
                echo "<img class='blog_banner' src='images/blog-img.png' alt='Default Blog Image'>";
            }
            echo "<h2 class='post-title'>{$post['title']}</h2>";
            $profilePicturePath = $post['profile_picture_path'];
            echo "<div class='author_info'>";
            if(!$profilePicturePath) {
                echo "<img class='author_profile' src='images/default_no_profile.avif' alt='' class='profile-picture'>";
            } else {
            echo "<img class='author_profile' src='$profilePicturePath' alt='Profile Picture' class='profile-pic-mini'>";
            }
            echo "<p class='post-meta'>Posted by {$post['name']} {$post['surname']} on <span class='date'>{$post['created_at']}</span></p>";
            echo "</div>";            $limitedContent = substr($post['content'], 0, 150);
            if (strlen($post['content']) > 150) {
                $limitedContent .= "...<a href='view_edit_post.php?id={$post['post_id']}'>See More!</a>"; // Add ellipsis if content is longer than 50 characters
            } else {
                $limitedContent .= "...<a href='view_edit_post.php?id={$post['post_id']}'>See More!</a>";
            }
            
            echo "<div class='post-content'>$limitedContent</div>";
           
            echo "</div>";
        }}
        ?>
    </div>
    <?php 
    include "inc/blog_sidebar.php";
    ?>
</section>